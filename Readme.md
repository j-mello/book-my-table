# Book my table

## Fichiers de configuration

 - Copier .env.example dans .env
 - Copier api/.env-example dans api/.env
 - Copier les .env.example dans .env pour chacun des dossiers : client, mailer_api, note_api, notification_api, menu_api
 - Dans le .env du mailer_api, ajoutez les bonnes informations SMTP

## Installer les dépendances
 - vuejs => docker-compose run client npm install
 Pour l'API, rajoutez dans le network de l'image php du docker-compose : - exterior
 (A retirer une fois exécuté)
 - api => docker-compose run php composer install
 - Micro-service menu => docker-compose run menu_api npm install
 - Micro-service mailer => docker-compose run mailer_api npm install
 - Micro-service notes => docker-compose run note_api npm install
 - Micro-service notifications => docker-compose run notification_api npm install

## Générer la base de données
 Après le premier docker-compose up -d :
 - docker-compose exec php bin/console d:m:m (générer la base de données)
 - docker-compose exec php bin/console d:f:l (générer les fixtures)

## Créer/supprimer/lister les admins

- Créer un compte admin : docker-compose exec php bin/console admin:create admin@admin.com firstname lastname unMotDePasse
- Supprimer un compte admin : docker-compose exec php bin/console admin:delete emailDuCompteAsupprimer
- Lister les comptes admin : docker-compose exec php bin/console admin:list

## Pour le JWT

 - Générer les clés : docker-compose exec php bin/console lexik:jwt:generate-keypair
 - Tester le login :  curl -kX POST -H "Content-Type: application/ld+json" http://localhost:81/login -d '{"email":"votreEmail","password":"votrePasswordNonHashé"}'
   ######(Attention, si vous êtes sur Windows, cette commande ne fonctionne correctement que depuis git bash)

## Se connecter
  - Lancer docker-compose exec php bin/console d:f:l
  - Pour se connecter sur le vue, utilisez les logins suivants :
    - User normal : user@user.com / 1234
    - Un restaurateur : restorer@restorer.com / 1234
    - Un admin : admin@admin.com / 1234
