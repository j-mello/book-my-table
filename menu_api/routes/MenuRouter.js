const { Router } = require("express");
const {findMenu, findElement, deleteMenu, deleteElement, findTodaysElement, decaleMenu, decaleElement} = require("../libs/menuFinder");
const checkConnectedMiddleWare = require("../Middlewares/checkConnectedMiddleWare");
const checkRoleMiddleWare = require("../Middlewares/checkRoleMiddleWare");
const checkRestaurantOwnerMiddleWare = require("../Middlewares/checkRestaurantOwnerMiddleWare");

const limitNestedMenus = 3;

const router = Router();

router.use(checkConnectedMiddleWare);
router.use(checkRoleMiddleWare('ROLE_RESTORER'));

router.post("/:restaurant_id", checkRestaurantOwnerMiddleWare, async (req, res) => {
    try {
        if (req.body._id) delete req.body._id;
        req.restaurant.menus.push(req.body);
        await req.restaurant.save();

        res.status(201).json(req.restaurant.menus[req.restaurant.menus.length - 1]);
    } catch (e) {
        console.error(e);
        res.sendStatus(400);
    }
});

router.post("/:restaurant_id/:menu_id", checkRestaurantOwnerMiddleWare, async (req, res) => {
    try {
        let level = {level: 0};
        const menu = findMenu(req.restaurant, req.params.menu_id, level);
        if (menu === null) {
            return res.sendStatus(404);
        }

        if (level.level >= limitNestedMenus) {
            return res.status(409).json({detail: "Vous atteint la limite d'imbrication des menus"});
        }

        if (menu.elements.length > 0) {
            return res.sendStatus(409);
        }

        if (req.body._id) delete req.body._id;
        console.log(req.body);
        menu.subMenus.push(req.body);
        await req.restaurant.save();

        res.status(201).json(menu.subMenus[menu.subMenus.length - 1]);
    } catch (e) {
        console.error(e);
        res.sendStatus(400);
    }
});

router.put("/:restaurant_id/:menu_id/up", checkRestaurantOwnerMiddleWare, async (req, res) => {
    try {
        if (!decaleMenu(req.restaurant,req.params.menu_id, -1)) {
            return res.sendStatus(404);
        }
        await req.restaurant.save();
        res.sendStatus(200);
    } catch (e) {
        console.error(e);
        res.sendStatus(400);
    }
});

router.put("/:restaurant_id/:menu_id/down", checkRestaurantOwnerMiddleWare, async (req, res) => {
    try {
        if (!decaleMenu(req.restaurant,req.params.menu_id, 1)) {
            return res.sendStatus(404);
        }
        await req.restaurant.save();
        res.sendStatus(200);
    } catch (e) {
        console.error(e);
        res.sendStatus(400);
    }
});

router.put("/:restaurant_id/:menu_id", checkRestaurantOwnerMiddleWare, async (req,res) => {
    try {
        const menu = findMenu(req.restaurant, req.params.menu_id);
        if (menu === null) {
            return res.sendStatus(404);
        }

        menu.name = req.body.name;
        await req.restaurant.save();
        res.status(200).json(menu);
    } catch (e) {
        console.error(e);
        res.sendStatus(400);
    }
});

router.delete("/:restaurant_id/:menu_id", checkRestaurantOwnerMiddleWare, async (req, res) => {
    try {
        if (!deleteMenu(req.restaurant, req.params.menu_id)) {
            return res.sendStatus(404);
        }

        await req.restaurant.save();
        res.sendStatus(204);
    } catch (e) {
        console.error(e);
        res.sendStatus(400);
    }
});

router.post("/:restaurant_id/:menu_id/elements", checkRestaurantOwnerMiddleWare, async (req, res) => {
    try {
        let todaysElement;
        if (req.body.todays && (todaysElement = findTodaysElement(req.restaurant, req.params.menu_id))) {
            todaysElement.todays = false;
        }

        const menu = findMenu(req.restaurant, req.params.menu_id);
        if (menu === null) {
            return res.sendStatus(404);
        }

        if (menu.subMenus.length > 0) {
            return res.sendStatus(409);
        }

        if (req.body._id) delete req.body._id;
        menu.elements.push(req.body);

        await req.restaurant.save();
        res.status(201).json(menu.elements[menu.elements.length - 1]);
    } catch (e) {
        console.error(e);
        res.sendStatus(400);
    }
});

router.put("/:restaurant_id/:menu_id/elements/:element_id/up", checkRestaurantOwnerMiddleWare, async (req, res) => {
    try {
        if (!decaleElement(req.restaurant, req.params.menu_id, req.params.element_id, -1)) {
            return res.sendStatus(404);
        }
        await req.restaurant.save();
        res.sendStatus(200);
    } catch (e) {
        console.error(e);
        res.sendStatus(400);
    }
});

router.put("/:restaurant_id/:menu_id/elements/:element_id/down", checkRestaurantOwnerMiddleWare, async (req, res) => {
    try {
        if (!decaleElement(req.restaurant, req.params.menu_id, req.params.element_id, 1)) {
            return res.sendStatus(404);
        }
        await req.restaurant.save();
        res.sendStatus(200);
    } catch (e) {
        console.error(e);
        res.sendStatus(400);
    }
});

router.put("/:restaurant_id/:menu_id/elements/:element_id", checkRestaurantOwnerMiddleWare, async (req, res) => {
    try {
        let todaysElement;
        if (req.body.todays && (todaysElement = findTodaysElement(req.restaurant, req.params.menu_id))) {
            todaysElement.todays = false;
        }

        let element = findElement(req.restaurant, req.params.menu_id, req.params.element_id);

        if (!element) {
            return res.sendStatus(404);
        }

        for (const [key, value] of Object.entries(req.body)) {
            if (key !== '_id' && element[key] !== undefined)
                element[key] = value;
        }

        await req.restaurant.save();
        res.status(200).json(element);
    } catch (e) {
        console.error(e);
        res.sendStatus(400);
    }
});

router.delete("/:restaurant_id/:menu_id/elements/:element_id", checkRestaurantOwnerMiddleWare, async (req, res) => {
    try {
        if (!deleteElement(req.restaurant, req.params.menu_id, req.params.element_id)) {
            return res.sendStatus(404);
        }

        await req.restaurant.save();
        res.sendStatus(204);
    } catch (e) {
        console.error(e);
        res.sendStatus(400);
    }
});

module.exports = router;
