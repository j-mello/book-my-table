const { Router } = require("express");
const Restaurant = require("../Models/Restaurant");

const router = Router();

router.get("/:id", async (req,res) => {
    try {
        const restaurant = await Restaurant.findOne({id: req.params.id});
        if (restaurant === null) {
            return res.sendStatus(404);
        }
        res.status(200).json({...restaurant._doc, ownerMail: undefined});
    } catch(e) {
        console.error(e);
        res.sendStatus(400);
    }
});


// Theses routes are only accessibles from internal APIs

router.post("/", async (req,res) => {
    if (!req.body.id || !req.body.ownerMail || !req.body.name) {
        return res.sendStatus(400);
    }
    try {
        let restaurant = await Restaurant.findOne({id: req.body.id});
        if (restaurant !== null) {
            await restaurant.remove();
        }
        await Restaurant.create(req.body);
        res.sendStatus(201);
    } catch (e) {
        console.error(e);
        return res.sendStatus(400);
    }
});

router.put("/:id", async (req,res) => {
    if (!req.body.name) {
        return res.sendStatus(400);
    }

    try {
        const restaurant = await Restaurant.findOne({id: req.params.id});
        if (restaurant === null) {
            return res.sendStatus(404);
        }
        restaurant.name = req.body.name;
        await restaurant.save();
        res.sendStatus(204);
    } catch (e) {
        console.error(e);
        return res.sendStatus(400);
    }
});

router.delete("/:id", async (req,res) => {
    try {
        const restaurant = await Restaurant.findOne({id: req.params.id});
        if (restaurant === null) {
            return res.sendStatus(404);
        }
        await restaurant.remove();
        res.sendStatus(204);
    } catch (e) {
        console.error(e);
        return res.sendStatus(400);
    }
});

module.exports = router;
