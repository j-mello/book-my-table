const Restaurant = require("../Models/Restaurant");

module.exports = async function checkRestaurantOwnerMiddleWare(req,res,next) {
    if (!req.user) {
        return res.sendStatus(401);
    }

    const restaurant = await Restaurant.findOne({id: req.params.restaurant_id});
    if (restaurant === null) {
        return res.sendStatus(404);
    }

    if (restaurant.ownerMail !== req.user.username) {
        return res.sendStatus(403);
    }

    req.restaurant = restaurant;

    next();
}