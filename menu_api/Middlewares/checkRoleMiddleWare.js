const checkRoleMiddleWare = (roles) => (req,res,next) => {
    if (!(roles instanceof Array))
        roles = [roles];

    if (!req.user || roles.some(role => !req.user.roles.includes(role))) {
        return res.sendStatus(401);
    }

    next();
}

module.exports = checkRoleMiddleWare;