const { Schema } = require("mongoose");
const { connect } = require("../Mongo");

const db = connect();

const ElementSchema = new Schema({
	name: { type: String, required: true },
	description: { type: String, required: true },
	price: { type: Number, required: true },
	todays: { type: Boolean, required: true, default: false } // Si plat du jour
});

const MenuSchema = new Schema({
	name: { type: String, required: true },
	elements: [ElementSchema]
});

MenuSchema.add({
	subMenus: [MenuSchema]
})

const RestaurantSchema = new Schema({
	id: { type: Number, required: true },
	ownerMail: {type: String, required: true},
	name: { type: String, required: true },
	menus: [MenuSchema]
});

// @ts-ignore
module.exports = db.model('Restaurant', RestaurantSchema);
