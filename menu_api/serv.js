const express = require("express");
const cors = require("cors");
const RestaurantRouter = require("./routes/RestaurantRouter");
const MenuRouter = require("./routes/MenuRouter");

const app = express();

app.use(cors());
app.use(express.json());

app.use("/restaurants", RestaurantRouter);
app.use("/menus", MenuRouter);

app.listen(process.env.PORT || 3000, () => console.log("Http menu_api server started"));