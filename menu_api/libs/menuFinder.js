// increment and decrement level in an object passed by reference, because of a wtf bug when return menu and level in one time
function findMenu(parent, menuId, level = null) {
    for (let menu of parent.menus??parent.subMenus) {
        if (menu._id.toString() === menuId) {
            return menu;
        } else {
            if (level)
                level.level += 1;
            const foundMenu = findMenu(menu,menuId,level);
            if (foundMenu) {
                return foundMenu;
            }
            if (level)
                level.level -= 1;
        }
    }
    return null;
}

function deleteMenu(parent, menuId) {
    const menus = parent.menus??parent.subMenus;
    for (let i=0;i<menus.length;i++) {
        const menu = menus[i];
        if (menu._id.toString() === menuId) {
            menus.splice(i,1);
            return true;
        }
        if (deleteMenu(menu, menuId))
            return true;
    }
    return false;
}

function findElement(parent, menuId, elementId) {
    if (parent.elements && parent._id.toString() === menuId) {
        for (const element of parent.elements) {
            if (element._id.toString() === elementId)
                return element;
        }
        return null;
    }
    for (let subMenu of parent.subMenus??parent.menus) {
        let element = findElement(subMenu,menuId,elementId);
        if (element)
            return element;
    }
    return null;
}

function deleteElement(parent, menuId, elementId) {
    if (parent.elements && parent._id.toString() === menuId) {
        for (let i=0;i<parent.elements.length;i++) {
            const element = parent.elements[i];
            if (element._id.toString() === elementId) {
                parent.elements.splice(i,1);
                return true;
            }
        }
        return false;
    }
    for (const menu of parent.menus??parent.subMenus) {
        if (deleteElement(menu,menuId,elementId)) {
            return true;
        }
    }
    return false;
}

function findTodaysElement(parent, menuId) {
    if (parent.elements && parent._id.toString() === menuId) {
        for (const element of parent.elements) {
            if (element.todays)
                return element;
        }
        return null;
    }
    for (let subMenu of parent.subMenus??parent.menus) {
        let element = findTodaysElement(subMenu,menuId);
        if (element)
            return element;
    }
    return null;
}

function decaleElement(parent, menuId, elementId, toDecale) {
    if (parent.elements && parent._id.toString() === menuId) {
        for (let i=0;i<parent.elements.length;i++) {
            const element = parent.elements[i];
            if (element._id.toString() === elementId) {
                if (i+toDecale < 0 || i+toDecale >= parent.elements.length)
                    return parent;
                parent.elements.splice(i,1);
                parent.elements.splice(i+toDecale,0,element);
                return parent;
            }
        }
        return false
    }
    for (const menu of parent.menus??parent.subMenus) {
        const foundParent = decaleElement(menu,menuId,elementId,toDecale);
        if (foundParent) {
            return foundParent;
        }
    }
    return false;
}

function decaleMenu(parent,menuId, toDecale) {
    const menus = parent.menus??parent.subMenus;
    for (let i=0;i<menus.length;i++) {
        const menu = menus[i];
        if (menu.id === menuId) {
            if (i+toDecale < 0 || i+toDecale >= menus.length)
                return parent;
            menus.splice(i,1);
            menus.splice(i+toDecale, 0, menu);
            return parent;
        }
        const founddParent = decaleMenu(menu,menuId,toDecale);
        if (founddParent) {
            return founddParent;
        }
    }
    return false;
}


module.exports = { findMenu, deleteMenu, findElement, deleteElement, findTodaysElement, decaleMenu, decaleElement };
