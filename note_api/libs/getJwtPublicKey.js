const fs = require("fs/promises");
const fileExists = require("./fileExists");

const path = __dirname+"/../jwt/public.pem"
let publicKey;

module.exports = async function getJwtPublicKey() {
	if (!publicKey) {
		if (!await fileExists(path)) {
			throw new Error("public.pem not found");
		}

		publicKey = await fs.readFile(path).then(res => res.toString());
	}

	return publicKey
}
