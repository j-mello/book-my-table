const Restaurant = require("../Models/Restaurant");

const isNumber = n => parseInt(n).toString() === n && n !== "NaN";

module.exports = async function checkRestaurantMiddleWare(req,res,next) {
    const {restaurantId} = req.params;
    if (!isNumber(restaurantId))
        return res.sendStatus(404);

    const restaurant = await Restaurant.findOne({restaurantId});
    if (restaurant === null) {
        return res.sendStatus(404);
    }
    req.restaurant = restaurant;

    next();
}
