const { Schema } = require("mongoose");
const { connect } = require("../Mongo");

const db = connect();

const NoteSchema = new Schema({
	username: { type: String, required: true },
	note: { type: Number, required: true }
});

const RestaurantSchema = new Schema({
	restaurantId: { type: Number, required: true },
	notes: [NoteSchema]
});

// @ts-ignore
module.exports = db.model('Restaurant', RestaurantSchema);
