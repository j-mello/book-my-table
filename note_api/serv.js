const express = require("express");
const cors = require("cors");
const Restaurant = require("./Models/Restaurant");
const checkConnectedMiddleWare = require("./Middlewares/checkConnectedMiddleWare");
const checkRestaurantMiddleWare = require("./Middlewares/checkRestaurantMiddleWare");

const app = express();

app.use(cors());
app.use(express.json());


// This route in only accessible from internal APIs
app.post("//:restaurantId", async (req,res) => {
    const restaurantId = parseInt(req.params.restaurantId);
    const restaurant = await Restaurant.findOne({restaurantId});
    if (restaurant !== null) {
        await restaurant.remove();
    }
    await Restaurant.create({restaurantId});
    res.sendStatus(201);
});

app.delete("//:restaurantId", checkRestaurantMiddleWare, async (req, res) => {
    await req.restaurant.remove();
    res.sendStatus(204);
})


// This route is accessible from everyone
app.get("//:restaurantId/average", checkRestaurantMiddleWare, (req,res) => {
    const average = req.restaurant.notes.length > 0 ? req.restaurant.notes.reduce((acc,{note}) =>
        acc+note
        , 0)/req.restaurant.notes.length : null;
    res.status(200).json(average ? Math.round(average*2)/2 : null);
});

// These routes are accessible from external calls, if user is connected
app.use(checkConnectedMiddleWare);

app.get("//:restaurantId/note", checkRestaurantMiddleWare, (req,res) => {
    let noteObj;
    res.status(200).json(
        (noteObj = req.restaurant.notes.find(({username}) => username === req.user.username)) ? noteObj.note : null
    );
});

app.put("//:restaurantId/note", checkRestaurantMiddleWare, async (req,res) => {
    let {note} = req.body;
    if (note === undefined || typeof(note) !== "number")
        return res.sendStatus(400);

    if (note > 5)
        note = 5;
    if (note < 1)
        note = 1;

    note = Math.round(note*2)/2;

    const noteObj = req.restaurant.notes.find(({username}) => username === req.user.username);
    if (noteObj) {
        noteObj.note = note;
    } else {
        req.restaurant.notes.push({username: req.user.username, note})
    }

    await req.restaurant.save();
    res.sendStatus(200);
});

app.delete("//:restaurantId/note", checkRestaurantMiddleWare, async (req,res) => {
    req.restaurant.notes = req.restaurant.notes.filter(({username}) => username !== req.user.username);
    await req.restaurant.save();
    res.sendStatus(204);
});


app.listen(process.env.PORT || 3000, () => console.log("Http note_api server started"));
