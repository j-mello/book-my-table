<?php


namespace App\EventListener;


use App\Entity\Restaurant;
use App\Entity\User;
use App\Service\RestaurantConfirmationService;
use App\Service\RestaurantNoteService;
use App\Service\ScheduleCheckerService;
use App\Service\RestaurantMenuUpdaterService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\Security\Core\Security;

class RestaurantSubscriber implements EventSubscriber
{
    private $restaurantConfirmationService;
    private $scheduleCheckerService;
    private $restaurantMenuUpdaterService;
    private $restaurantNoteService;
    private $security;
    private $request;
    private $em;


    public function __construct(
    	RestaurantConfirmationService $restaurantConfirmationService,
		ScheduleCheckerService $scheduleCheckerService,
		RestaurantMenuUpdaterService $restaurantMenuUpdaterService,
		RestaurantNoteService $restaurantNoteService,
		Security $security,
		RequestStack $requestStack,
		EntityManagerInterface $em)
    {
        $this->restaurantConfirmationService = $restaurantConfirmationService;
        $this->scheduleCheckerService = $scheduleCheckerService;
        $this->restaurantMenuUpdaterService = $restaurantMenuUpdaterService;
        $this->restaurantNoteService = $restaurantNoteService;

        $this->security = $security;
        $this->request = $requestStack->getCurrentRequest();
        $this->em = $em;
    }

    public function getSubscribedEvents()
    {
        return [Events::prePersist, Events::postPersist, Events::preRemove, Events::preUpdate];
    }

	public function preUpdate(PreUpdateEventArgs $args): void
	{
		$this->checkSchedulesNbPlace($args);
		$this->updateRestaurantInMenuApi($args);
	}

    public function preRemove(LifecycleEventArgs $args): void
    {
        $this->sendDeletionNotificationAndMailByAdmin($args);
        $this->deleteRestaurantInMenuApi($args);
        $this->deleteRestaurantInNoteApi($args);
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $this->defineOwner($args);
    }

    public function postPersist(LifecycleEventArgs $args) {
        $this->sendConfirmationNotificationAndMail($args);
    }

    private function updateRestaurantInMenuApi(PreUpdateEventArgs $args): void {
		$restaurant = $args->getObject();
		$changeSet = $args->getEntityChangeSet();

    	if ($restaurant instanceof Restaurant && isset($changeSet['name'])) {
			$this->restaurantMenuUpdaterService->updateRestaurantInMenuAPI($restaurant);
		}
	}

    private function checkSchedulesNbPlace(PreUpdateEventArgs $args) {
		$restaurant = $args->getObject();
		$changeSet = $args->getEntityChangeSet();

		if ($restaurant instanceof Restaurant && isset($changeSet['nbPlace'])) {
			foreach ($restaurant->getSchedules() as $schedule) {
				if ($schedule->getNbPlace() > $restaurant->getNbPlace()) {
					$schedule->setNbPlace($restaurant->getNbPlace());
					if (!$this->scheduleCheckerService->checkScheduleNbPlace($schedule))
						throw new ConflictHttpException("Vous ne pouvez pas diminuer le nombre places du service '".$schedule->getName()."' , elles sont prises par des réservations");

					$this->em->flush();
				}
			}
		}
	}

	private function deleteRestaurantInMenuApi(LifecycleEventArgs $args) {
		$entity = $args->getObject();

		if ($entity instanceof Restaurant && $entity->getValidated()) {
			$this->restaurantMenuUpdaterService->deleteRestaurantInMenuAPI($entity);
		}
	}

	private function deleteRestaurantInNoteApi(LifecycleEventArgs $args) {
		$entity = $args->getObject();

		if ($entity instanceof Restaurant && $entity->getValidated()) {
			$this->restaurantNoteService->deleteRestaurantInNoteApi($entity);
		}
	}

    private function sendDeletionNotificationAndMailByAdmin(LifecycleEventArgs $args) {
        $entity = $args->getObject();
        if ($entity instanceof Restaurant && in_array(User::ROLE_ADMIN, $this->security->getUser()->getRoles()) && $entity->getValidated()) {
            $content = json_decode($this->request->getContent());
            if ($content === null || !property_exists($content, 'reason') || trim($content->reason) === "") {
                throw new BadRequestException("Vous devez spécifier une raison");
            }
            $this->restaurantConfirmationService->sendRestaurantDeletedByAdmin($entity, trim($content->reason));
            $this->restaurantConfirmationService->sendRestaurantDeletedByAdminNotification($entity);
        }
    }

    private function sendConfirmationNotificationAndMail(LifecycleEventArgs $args) {
        $entity = $args->getObject();

        if (!$entity instanceof Restaurant || $entity->getValidated()) {
            return;
        }

        $this->restaurantConfirmationService->sendRestaurantCreatedMail($entity);
        $this->restaurantConfirmationService->sendRestaurantCreatedNotification($entity);
    }

    private function defineOwner(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof Restaurant) {
            return;
        }

        if (is_null($entity->getOwner()) && !is_null($this->security->getUser())) {
            $entity->setOwner($this->security->getUser());
            $entity->addUser($this->security->getUser());
        }
    }
}
