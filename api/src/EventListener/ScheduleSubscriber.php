<?php


namespace App\EventListener;

use App\Entity\Schedule;
use App\Repository\RestaurantRepository;
use App\Service\ScheduleCheckerService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

class ScheduleSubscriber implements EventSubscriber
{

	private $scheduleCheckerService;
	private $restaurantRepository;

	public function __construct(ScheduleCheckerService $scheduleCheckerService, RestaurantRepository $restaurantRepository) {
		$this->scheduleCheckerService = $scheduleCheckerService;
		$this->restaurantRepository = $restaurantRepository;
	}

    public function getSubscribedEvents()
    {
        return [Events::preRemove, Events::prePersist];
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        if ($entity instanceof Schedule) {
            $entity->setRestaurant($this->restaurantRepository->find($entity->getRestaurantId()));
        }
    }

    public function preRemove(LifecycleEventArgs $args): void
    {
    	$entity = $args->getObject();
    	if ($entity instanceof Schedule && !$this->scheduleCheckerService->checkScheduleForDelete($entity)) {
    	    throw new ConflictHttpException("Vous ne pouvez pas supprimer le service '".$entity->getName()."', des réservations sont prises dessus");
    	}
    }
}
