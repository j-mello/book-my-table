<?php


namespace App\EventListener;


use App\Entity\User;
use App\Service\AccountConfirmationService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserSubscriber implements EventSubscriber
{
    private $accountConfirmationService;
    private $passwordHasher;
    private $request;

    public function __construct(UserPasswordHasherInterface $passwordHasher, AccountConfirmationService $accountConfirmationService, RequestStack $requestStack)
    {
        $this->passwordHasher = $passwordHasher;
        $this->accountConfirmationService = $accountConfirmationService;
        $this->request = $requestStack->getCurrentRequest();
    }

    public function getSubscribedEvents()
    {
        return [Events::prePersist, Events::postPersist, Events::preUpdate];
    }

    public function preUpdate(PreUpdateEventArgs $args): void
    {
    	$this->banOrValidate($args);
        $this->encodePassword($args);
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $this->checkRole($args);
        $this->encodePassword($args);
    }

    public function postPersist(LifecycleEventArgs $args): void {
        $this->sendConfirmationMail($args);
    }

	/**
	 * @param PreUpdateEventArgs $args
	 * desc Envoie un mail quand on valide un restaurateur, ban, on deban un utilisateur
	 */
    private function banOrValidate(PreUpdateEventArgs $args) {
		$user = $args->getObject();
		$changeSet = $args->getEntityChangeSet();

		if (!$user instanceof User ||
			(!array_key_exists('restorerValidated', $changeSet) && !array_key_exists('banned', $changeSet))) {
			return;
		}

		$content = json_decode($this->request->getContent());
		$reason = ($content && property_exists($content,'reason')) ? $content->reason : null;

		if (isset($changeSet['restorerValidated']) && !in_array(User::ROLE_RESTORER, $user->getRoles())) {
			throw new BadRequestException("Vous ne pouvez pas valider ou dévalider un utilisateur qui n'est pas un restaurateur");
		}

		if (isset($changeSet['restorerValidated']) && $changeSet['restorerValidated'][1] === false) {
			throw new BadRequestException("Vous ne pouvez pas dévalider un restaurateur");
		}

		if (isset($changeSet['banned']) && $changeSet['banned'][1] && $reason === null) {
			throw new BadRequestException("Vous ne pouvez pas bannir un utilisateur sans donner une raison");
		}

		if (isset($changeSet['restorerValidated'])) {
			$this->accountConfirmationService->sendRestorerValidatedMail($user);
		}
		if (isset($changeSet['banned'])) {
			if ($changeSet['banned'][1]) {
				$this->accountConfirmationService->sendBannedMail($user,$reason);
			} else {
				$this->accountConfirmationService->sendUnBannedMail($user, $reason);
			}
		}

	}

	/**
	 * @param LifecycleEventArgs $args
	 * desc Vérifie si le rôle admin n'est pas défini au moment de l'inscription
	 */
    private function checkRole(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof User || $entity->getRegisterToken() === null) {
            return;
        }

        if (in_array(User::ROLE_ADMIN,$entity->getRoles())) {
            throw new AccessDeniedHttpException("Vous n'avez pas le droit de créer un compte admin");
        }
    }

	/**
	 * @param LifecycleEventArgs $args
	 * desc Envoie un mail à la création du compte
	 */
    private function sendConfirmationMail(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof User || $entity->getRegisterToken() === null) {
            return;
        }

        $this->accountConfirmationService->sendConfirmAccountMail($entity);
    }

	/**
	 * @param LifecycleEventArgs $args
	 * desc Hash automatiquement le mot de passe
	 */
    private function encodePassword(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof User) {
            return;
        }

        if (!empty($entity->getPlainPassword())) {
            $entity->setPassword($this->passwordHasher->hashPassword($entity, $entity->getPlainPassword()));
        }
    }
}
