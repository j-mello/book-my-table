<?php


namespace App\EventListener;


use App\Entity\Reservation;
use App\Repository\UserRepository;
use App\Repository\ScheduleRepository;
use App\Service\ScheduleCheckerService;
use App\Service\ReservationConfirmationService;
use App\Entity\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\Security\Core\Security;

class ReservationSubscriber implements EventSubscriber
{
    private $userRepository;
    private $scheduleRepository;

    private $reservationConfirmationService;
    private $security;
    private $request;

    public function __construct(UserRepository $userRepository, ReservationConfirmationService $reservationConfirmationService , ScheduleRepository $scheduleRepository, Security $security, RequestStack $requestStack)
    {
        $this->userRepository = $userRepository;
        $this->scheduleRepository = $scheduleRepository;
        $this->reservationConfirmationService = $reservationConfirmationService;
        $this->security = $security;
        $this->request = $requestStack->getCurrentRequest();
    }

    public function getSubscribedEvents()
    {
        return [Events::prePersist, Events::preRemove, Events::preUpdate, Events::postPersist];
    }

	public function preUpdate(PreUpdateEventArgs $args): void
	{
		$this->defineScheduleAndRestaurant($args);
	}

    public function preRemove(LifecycleEventArgs $args): void
    {
        if (!in_array(explode("/",$this->request->getUri())[3], ["schedules", "restaurants"]))
            $this->sendDeletionMailAndNotificationByRestorer($args);
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $this->defineCustomer($args);
        $this->defineScheduleAndRestaurant($args);
        $entity = $args->getObject();
        if ($entity instanceof Reservation && $entity->getEmailToConfirm() === '') {
            $entity->setEmailToConfirm(null);
        }
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $this->sendReservationConfirmationMailAndNotification($args);
    }

    private function defineCustomer(LifecycleEventArgs $args)
    {
        $reservation = $args->getObject();

        if ($reservation instanceof Reservation && $reservation->getSpecifiedCustomerID() != null)
        {
            $customer = $this->userRepository->find($reservation->getSpecifiedCustomerID());
            if ($customer == null)
            {
                throw new BadRequestException("Le customer n'existe pas !");
            }
            $reservation->setCustomer($customer);
		}
    }

    private function defineScheduleAndRestaurant(LifecycleEventArgs $args)
    {
        $reservation = $args->getObject();

        if ($reservation instanceof Reservation && ($reservation->getScheduleID() != null || $reservation->getSchedule() == null || $reservation->getRestaurant() == null))
        {
            if ($reservation->getScheduleID() == null)
            {
                throw new BadRequestException("Le schedule n'est pas mentionné !");
            }
            $schedule = $this->scheduleRepository->find($reservation->getScheduleID());

            if ($schedule == null)
            {
                throw new BadRequestException("Le schedule n'existe pas margoulin");
            }
            $restaurant = $schedule->getRestaurant();

            $reservation->setSchedule($schedule);
            $reservation->setRestaurant($restaurant);
        }
    }

    private function sendDeletionMailAndNotificationByRestorer($args)
    {
        $currentUser = $this->security->getUser();

        $entity = $args->getObject();

		if ($currentUser === null || !($entity instanceof Reservation) || ($entity->getCustomer() === null && $entity->getEmailToConfirm() === null)) {
			return;
		}

		$schedule = $entity->getSchedule();
		if ($schedule === null && $entity->getScheduleID() !== null) {
			$schedule = $this->scheduleRepository->find($entity->getScheduleID());
		}
		if ($schedule === null) {
			return;
		}
		$reservationMaxDateAndTime = new \DateTime($entity->getDate()->format("Y-m-d")." ".$schedule->getEnding()->format("H:i"));

        if (
			(new \DateTime('now')) <= $reservationMaxDateAndTime &&
			in_array(User::ROLE_RESTORER, $currentUser->getRoles())) {
                if ($entity->getCustomer() !== null && $entity->getCustomer()->getId() !== $currentUser->getId())
                    $this->reservationConfirmationService->sendReservationDeletedByRestorerNotification($entity);

                if (
                    ($entity->getCustomer() !== null && $entity->getCustomer()->getId() !== $currentUser->getId()) ||
                    ($entity->getEmailToConfirm() !== null && $entity->getCustomer() === null)) {

                    $content = json_decode($this->request->getContent());
                    if ($content === null || !property_exists($content, 'reason') || trim($content->reason) == "") {
                        throw new BadRequestException("Vous devez spécifier une raison");
                    }
                    $this->reservationConfirmationService->sendReservationDeletedByRestorerMail($entity, trim($content->reason));
                }
        }
    }

    private function sendReservationConfirmationMailAndNotification($args)
    {
        $entity = $args->getObject();
        if ($entity instanceof Reservation && $this->security->getUser() !== null) {
            if ($entity->getCustomer() != null) {
                $this->reservationConfirmationService->sendReservationConfirmationNotification($entity);
            }
            if ($entity->getCustomer() != null || $entity->getEmailToConfirm() !== null)
                $this->reservationConfirmationService->sendReservationConfirmationMail($entity);

            $this->reservationConfirmationService->sendReservationReceivedNotification($entity);
        }
    }
}
