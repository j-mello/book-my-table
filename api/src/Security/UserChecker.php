<?php

namespace App\Security;

use App\Entity\User;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\Exception\LockedException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof User) {
            return;
        }

        if ($user->getBanned()) {
            throw new CustomUserMessageAccountStatusException("Vous êtes bannis");
        }
        if ($user->getRegisterToken() != null) {
            throw new CustomUserMessageAccountStatusException("Vous devez activer votre compte");
        }
        if (in_array(User::ROLE_RESTORER, $user->getRoles()) && !$user->getRestorerValidated()) {
            throw new CustomUserMessageAccountStatusException("Votre compte réstaurateur doit être validé par un administrateur");
        }
    }

    public function checkPostAuth(UserInterface $user)
    {
    }
}
