<?php

namespace App\Security\Voter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator;
use App\Entity\User;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class UserSubResourceVoter extends Voter
{
    private $request;
    private $entitySourceClass;
	private $subresourceClass;

    public function __construct(RequestStack $requestStack) {
        $this->request = $requestStack->getCurrentRequest();
    }

    protected function supports(string $attribute, $subject): bool
    {
    	if ($attribute !== 'VIEW_AS_USER_SUBRESOURCE' || !$subject instanceof Paginator) {
    		return false;
		}

    	$this->entitySourceClass = $this->request->attributes->get('_api_identifiers')['id'][0];
    	$this->subresourceClass = $this->request->attributes->get('_api_resource_class');
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return $this->entitySourceClass === User::class;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $connectedUser = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$connectedUser instanceof UserInterface) {
            return false;
        }

        $userId = $this->request->attributes->get("id");
        if ($userId === null || !is_numeric($userId)) {
            return false;
        }

        return (int)$userId === $connectedUser->getId();
    }
}
