<?php

namespace App\Security\Voter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator;
use App\Entity\Restaurant;
use App\Entity\RestaurantJoinRequest;
use App\Entity\RestaurantType;
use App\Entity\Schedule;
use App\Repository\RestaurantRepository;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class RestaurantSubResourceVoter extends Voter
{
    private $request;
    private $restaurantRepository;
    private $entitySourceClass;
	private $subresourceClass;

    public function __construct(RequestStack $requestStack, RestaurantRepository $restaurantRepository) {
        $this->restaurantRepository = $restaurantRepository;
        $this->request = $requestStack->getCurrentRequest();
    }

    protected function supports(string $attribute, $subject): bool
    {
    	if ($attribute !== 'VIEW_AS_RESTAURANT_SUBRESOURCE' || !$subject instanceof Paginator) {
    		return false;
		}

    	$this->entitySourceClass = $this->request->attributes->get('_api_identifiers')['id'][0];
    	$this->subresourceClass = $this->request->attributes->get('_api_resource_class');
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return $this->entitySourceClass === Restaurant::class;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        $restaurantId = $this->request->attributes->get("id");
        if ($restaurantId === null || !is_numeric($restaurantId)) {
            return false;
        }

        $restaurant = $this->restaurantRepository->find((int)$restaurantId);
        if ($restaurant == null) {
            return false;
        }
        
        return $restaurant->getValidated() &&
			(
			    $this->subresourceClass === RestaurantType::class ||
                $this->subresourceClass === Schedule::class ||
                (
                    $user instanceof UserInterface &&
                    ($this->subresourceClass === RestaurantJoinRequest::class && $user->getId() === $restaurant->getOwner()->getId()) ||
                    ($this->subresourceClass !== RestaurantJoinRequest::class && $restaurant->hasUser($user))
                )
            );
    }
}
