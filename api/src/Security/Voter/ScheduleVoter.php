<?php

namespace App\Security\Voter;

use App\Repository\RestaurantRepository;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\Schedule;
use App\Entity\User;

class ScheduleVoter extends Voter
{
    private $restaurantRepository;

    public function __construct(RestaurantRepository $restaurantRepository) {
        $this->restaurantRepository = $restaurantRepository;
    }

    protected function supports(string $attribute, $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['POST', 'GET', 'PUT', 'DELETE'])
            && $subject instanceof Schedule;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'POST':
                return $this->checkPost($subject, $user);
            case 'PUT':
                return $this->checkPut($subject, $user);
            case 'GET':
                return $this->checkGet($subject);
            case 'DELETE':
                return $this->checkDelete($subject, $user);
        }

        return false;
    }

    private function checkGet(Schedule $schedule) {
        return $schedule->getRestaurant()->getValidated();
    }

    private function checkPut(Schedule $schedule, User $user) {
        return in_array(User::ROLE_RESTORER, $user->getRoles())
            && $schedule->getRestaurant()->getValidated()
            && $schedule->getRestaurant()->getOwner()->getId() === $user->getId();
    }

    private function checkDelete(Schedule $schedule, User $user) {
        return in_array(User::ROLE_RESTORER, $user->getRoles())
            && $schedule->getRestaurant()->getValidated()
            && $schedule->getRestaurant()->getOwner()->getId() === $user->getId();
    }

    private function checkPost(Schedule $schedule, User $user) {
        $restaurant = $this->restaurantRepository->find($schedule->getRestaurantId());
        if ($restaurant === null) {
            throw new Exception("Ce restaurant n'existe pas");
        }
        return in_array(User::ROLE_RESTORER, $user->getRoles())
            && $restaurant->getValidated()
            && $restaurant->getOwner()->getId() === $user->getId();
    }
}

