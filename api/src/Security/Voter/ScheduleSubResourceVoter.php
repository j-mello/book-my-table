<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\Schedule;
use App\Repository\ScheduleRepository;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator;

class ScheduleSubResourceVoter extends Voter
{
    private $request;
    private $scheduleRepository;
    private $entitySourceClass;
	private $subresourceClass;

    public function __construct(RequestStack $requestStack, ScheduleRepository $scheduleRepository) {
        $this->scheduleRepository = $scheduleRepository;
        $this->request = $requestStack->getCurrentRequest();
    }

    protected function supports(string $attribute, $subject): bool
    {
    	if ($attribute !== 'VIEW_AS_SCHEDULE_SUBRESOURCE' || !$subject instanceof Paginator) {
    		return false;
		}
    	$this->entitySourceClass = $this->request->attributes->get('_api_identifiers')['id'][0];
    	$this->subresourceClass = $this->request->attributes->get('_api_resource_class');
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return $this->entitySourceClass === Schedule::class;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        $scheduleId = $this->request->attributes->get("id");
        if ($scheduleId === null || !is_numeric($scheduleId)) {
            return false;
        }

        $schedule = $this->scheduleRepository->find((int)$scheduleId);
        if ($schedule == null) {
            return false;
        }

        return $schedule->getRestaurant()->getValidated() &&
            $schedule->getRestaurant()->hasUser($user);
    }
}
