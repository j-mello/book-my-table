<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Repository\ScheduleRepository;
use App\Entity\User;
use App\Entity\Reservation;
use App\Entity\Restaurant;

class ReservationVoter extends Voter
{
    private $scheduleRepository;

    public function __construct(ScheduleRepository $scheduleRepository)
    {
        $this->scheduleRepository = $scheduleRepository;
    }

    protected function supports(string $attribute, $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['POST','PUT','DELETE'])
            && $subject instanceof Reservation;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'POST':
                return $this->checkPost($subject, $user);
            case 'PUT':
                return $this->checkPut($subject, $user);
            case 'DELETE':
                return $this->checkDelete($subject, $user);
        }

        return false;
    }

    private function checkPost(Reservation $reservation, User $user)
    {
        if ($reservation->getScheduleID() == null) {
            return false;
        }

        $schedule = $this->scheduleRepository->find($reservation->getScheduleID());

        if ($schedule == null) {
            return false;
        }
        $restaurant = $schedule->getRestaurant();
        return  (
                    in_array(User::ROLE_USER, $user->getRoles()) ||
                    in_array(User::ROLE_RESTORER, $user->getRoles())
                ) && (
                    ($restaurant->hasUser($user) && $reservation->getSpecifiedCustomerID() !== $user->getId()) ||
                    $reservation->getSpecifiedCustomerID() === $user->getId()
                );
    }

    private function checkPut(Reservation $reservation, User $user)
    {   
        return (
                    (
                        (   
                            $reservation->getCustomer() != null &&
                            $user->getId() == $reservation->getCustomer()->getId()
                        ) || 
                        (
                            in_array(User::ROLE_RESTORER, $user->getRoles()) &&
                            $reservation->getRestaurant()->hasUser($user)
                        )
                    ) &&
                    (
                        $reservation->getScheduleID() === null ||
                        $this->checkScheduleIDExistInRestaurant($reservation->getRestaurant(), $reservation->getScheduleID())
                    )
                ); 
    }

    private function checkDelete(Reservation $reservation, User $user)
    {
        return (
            (   
                $reservation->getCustomer() != null &&
                in_array(User::ROLE_USER, $user->getRoles()) &&
                $user->getId() == $reservation->getCustomer()->getId()
            ) || 
            (
                in_array(User::ROLE_RESTORER, $user->getRoles()) &&
                $reservation->getRestaurant()->hasUser($user)
            )  
        );
    }

    private function checkScheduleIDExistInRestaurant(Restaurant $restaurant, int $scheduleID)
    {
        foreach ($restaurant->getSchedules() as $schedule) 
        {
            if ($schedule->getId() == $scheduleID) return true;
        }
        return false;
    }
}
