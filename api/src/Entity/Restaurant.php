<?php

namespace App\Entity;

use App\Repository\RestaurantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Restaurant\ListRestaurantsController;
use App\Controller\Restaurant\ValidateRestaurantController;
use App\Controller\Restaurant\DenyRestaurantController;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\Restaurant\PutRestaurantTypesController;
use App\Controller\Restaurant\AskToJoinRestaurantController;
use App\Controller\Restaurant\BanRestaurantMemberController;
use App\Controller\Restaurant\CheckIfIsMemberInRestaurantController;
use App\Controller\Restaurant\LeaveRestaurantController;

/**
 * @ApiResource(
 *    itemOperations={
 *       "GET"={
 *           "normalization_context"={"groups"={"get_restaurant"}},
 *           "security"="is_granted('ROLE_ADMIN') || object.hasUser(user) || object.getValidated()"
 *        },
 *       "validate_restaurant"={
 *           "method"="POST",
 *           "path"="/restaurants/{id}/validate",
 *           "controller"= ValidateRestaurantController::class,
 *           "security"="is_granted('ROLE_ADMIN')",
 *           "normalization_context"={"groups"={"show_restaurant_validated"}},
 *           "denormalization_context"={"groups"={"validate_restaurant"}}
 *        },
 *        "deny_restaurant"={
 *           "method"="POST",
 *           "path"="/restaurants/{id}/deny",
 *           "controller"= DenyRestaurantController::class,
 *           "security"="is_granted('ROLE_ADMIN')",
 *           "normalization_context"={"groups"={"show_restaurant_denied"}},
 *           "denormalization_context"={"groups"={"deny_restaurant"}}
 *        },
 *        "DELETE"={
 *           "security"="is_granted('ROLE_ADMIN') || user == object.getOwner()",
 *           "openapi_context"={
 *                 "summary"="Delete a restaurant",
 *                 "requestBody"={
 *                     "content"={
 *                         "application/ld+json"={
 *                              "schema"={
 *                                  "type"="object",
 *                                  "properties"={
 *                                       "reason"={"type"="string"}
 *                                   }
 *                               }
 *                          }
 *                      }
 *                 }
 *             }
 *        },
 *       "PUT"={
 *          "security"="user == object.getOwner() && object.getValidated()",
 *          "denormalization_context"={"groups"={"put_restaurant"}},
 *          "normalization_context"={"groups"={"show_after_put_restaurant"}},
 *       },
 *       "put_restaurant_types"={
 *          "method"="PUT",
 *          "path"="/restaurants/{id}/types",
 *          "security"="user == object.getOwner() && object.getValidated()",
 *          "controller"= PutRestaurantTypesController::class,
 *          "denormalization_context"={"groups"={"patch_restaurant_types"}},
 *          "normalization_context"={"groups"={"show_after_patch_restaurant_types"}},
 *          "openapi_context"={
 *                 "summary"="Specify restaurant types id, separated by comma",
 *                 "requestBody"={
 *                     "content"={
 *                         "application/ld+json"={
 *                              "schema"={
 *                                  "type"="object",
 *                                  "properties"={
 *                                       "types"={"type"="string"}
 *                                   }
 *                               }
 *                          }
 *                      }
 *                 }
 *             }
 *       },
 *       "request_to_join"={
 *          "method"="POST",
 *          "path"="/restaurants/{id}/join",
 *          "security"="is_granted('ROLE_RESTORER') && object.getValidated() && !object.hasUser(user)",
 *          "controller"= AskToJoinRestaurantController::class,
 *          "denormalization_context"={"groups"={"request_to_join_restaurant"}},
 *          "normalization_context"={"groups"={"show_after_request_to_join_restaurant"}},
 *          "openapi_context"={
 *                 "summary"="Ask to join a restaurant",
 *                 "requestBody"={
 *                     "content"={
 *                         "application/ld+json"={
 *                              "schema"={
 *                                  "type"="object",
 *                                  "properties"={
 *                                       "reason"={"type"="string"}
 *                                   }
 *                               }
 *                          }
 *                      }
 *                 }
 *             }
 *       },
 *       "leave_restaurant"={
 *          "method"="DELETE",
 *          "path"="/restaurants/{id}/leave",
 *          "security"="is_granted('ROLE_RESTORER') && object.getValidated() && object.hasUser(user) && object.getOwner() != user",
 *          "controller"= LeaveRestaurantController::class,
 *          "openapi_context"={
 *                 "summary"="Leave a restaurant",
 *                 "requestBody"={
 *                     "content"={
 *                         "application/ld+json"={
 *                              "schema"={
 *                                  "type"="object",
 *                                  "properties"={
 *                                       "reason"={"type"="string"}
 *                                   }
 *                               }
 *                          }
 *                      }
 *                 }
 *             }
 *        },
 *       "ban_member_from_restaurant"={
 *          "method"="DELETE",
 *          "path"="/restaurants/{id}/member",
 *          "controller"= BanRestaurantMemberController::class,
 *          "security"="is_granted('ROLE_RESTORER') && object.getValidated() && object.getOwner() == user",
 *          "denormalization_context"={"groups"={"ban_member_restaurant"}},
 *          "normalization_context"={"groups"={"show_after_ban_member_restaurant"}},
 *          "openapi_context"={
 *                 "summary"="Ban a member from a restaurant",
 *                 "requestBody"={
 *                     "content"={
 *                         "application/ld+json"={
 *                              "schema"={
 *                                  "type"="object",
 *                                  "properties"={
 *                                       "restorerId"={"type"="integer"},
 *                                       "reason"={"type"="string"}
 *                                   }
 *                               }
 *                          }
 *                      }
 *                 }
 *             }
 *       },
 *       "is_member"={
 *          "method"="GET",
 *          "path"="/restaurants/{id}/is_member",
 *          "controller"= CheckIfIsMemberInRestaurantController::class,
 *          "security"="is_granted('ROLE_RESTORER') && object.getValidated()",
 *       }
 *    },
 *    collectionOperations={
 *       "get_all_restaurants"={
 *           "method"="GET",
 *           "path"="/restaurants/",
 *           "normalization_context"={"groups"={"get_all_restaurants"}},
 *           "controller"= ListRestaurantsController::class,
 *        },
 *        "POST"={
 *           "denormalization_context"={"groups"={"post_restaurant"}},
 *           "normalization_context"={"groups"="show_after_post_restaurant"},
 *           "security"="is_granted('ROLE_RESTORER')"
 *        }
 *    },
 *    subresourceOperations={
 *          "api_users_owned_restaurants_get_subresource"={
 *              "normalization_context"={"groups"={"get_all_restaurants"}},
 *              "security"="is_granted('VIEW_AS_USER_SUBRESOURCE', object)"
 *          },
 *          "api_users_restaurants_get_subresource"={
 *              "normalization_context"={"groups"={"get_all_restaurants"}},
 *              "security"="is_granted('VIEW_AS_USER_SUBRESOURCE', object)"
 *          }
 *    }
 * )
 * @ORM\Entity(repositoryClass=RestaurantRepository::class)
 */
class Restaurant
{
    /**
     * @Groups({"get_all_restaurants","get_restaurant","show_after_post_restaurant",
     * "show_restaurant_validated","read_schedule",
     * "get_restaurant_join_request", "show_reservation_after_post",
     * "show_reservation_after_put", "schedule_get_reservations", "user_get_reservations"})
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"get_all_restaurants","get_restaurant","post_restaurant",
     * "show_after_post_restaurant","show_restaurant_validated","put_restaurant",
     * "show_after_put_restaurant","read_schedule",
     * "get_restaurant_join_request", "show_reservation_after_post",
     * "show_reservation_after_put", "schedule_get_reservations", "user_get_reservations"})
     * @Assert\NotBlank
     * @Assert\Length(
     *     min = 2,
     *     max = 255,
     *     minMessage = "Le nom doit faire au moins {{ limit }} caractères",
     *     maxMessage = "Le nom ne peut pas faire plus de {{ limit }} caractères"
     * )
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Groups({"get_restaurant","post_restaurant","show_after_post_restaurant","show_restaurant_validated","put_restaurant","show_after_put_restaurant"})
     * @Assert\Range(
     *      min = 4,
     *      max = 5000,
     *      notInRangeMessage = "Un restaurant doit avoir entre {{ min }} et {{ max }} places",
     * )
     * @ORM\Column(type="integer")
     */
    private $nbPlace;

    /**
     * @Groups({"get_all_restaurants","get_restaurant","post_restaurant","show_after_post_restaurant","show_restaurant_validated","put_restaurant","show_after_put_restaurant"})
     * @Assert\NotBlank
     * @Assert\Length(
     *     min = 2,
     *     max = 50,
     *     minMessage = "Le nom de la ville doit faire au moins {{ limit }} caractères",
     *     maxMessage = "Le nom de la ville ne peut pas faire plus de {{ limit }} caractères"
     * )
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @Groups({"get_all_restaurants","get_restaurant","post_restaurant","show_after_post_restaurant","show_restaurant_validated","put_restaurant","show_after_put_restaurant"})
     * @Assert\NotBlank
     * @Assert\Length(
     *     min = 5,
     *     max = 6,
     *     minMessage = "Le code postal doit faire au moins {{ limit }} caractères",
     *     maxMessage = "Le code postal ne peut pas faire plus de {{ limit }} caractères"
     * )
     * @ORM\Column(type="string", length=6)
     */
    private $zipcode;

    /**
     * @Groups({"get_restaurant","post_restaurant","show_after_post_restaurant","show_restaurant_validated","put_restaurant","show_after_put_restaurant"})
     * @Assert\NotBlank
     * @Assert\Length(
     *     min = 2,
     *     max = 255,
     *     minMessage = "L'adresse doit faire au moins {{ limit }} caractères",
     *     maxMessage = "L'adresse ne peut pas faire plus de {{ limit }} caractères"
     * )
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @Groups({"get_restaurant","post_restaurant","show_after_post_restaurant","show_restaurant_validated","put_restaurant","show_after_put_restaurant"})
     * @Assert\NotBlank
     * @Assert\Length(
     *     min = 10,
     *     max = 20,
     *     minMessage = "Le numéro de téléphone doit faire au moins {{ limit }} caractères",
     *     maxMessage = "Le numéro de téléphone ne peut pas faire plus de {{ limit }} caractères"
     * )
     * @ORM\Column(type="string", length=20)
     */
    private $phonenumber;

    /**
     * @Groups({"get_all_restaurants","get_restaurant","show_after_patch_restaurant_types"})
     * @ApiSubresource()
     * @ORM\ManyToMany(targetEntity=RestaurantType::class, mappedBy="restaurants")
     */
    private $restaurantTypes;

    /**
     * @ApiSubresource()
     * @ORM\OneToMany(targetEntity=Schedule::class, mappedBy="restaurant", orphanRemoval=true)
     */
    private $schedules;

    /**
     * @ApiSubresource()
     * @ORM\OneToMany(targetEntity=Reservation::class, mappedBy="restaurant", orphanRemoval=true)
     */
    private $reservations;

    /**
     * @Groups({"get_all_restaurants","get_restaurant"})
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="ownedRestaurants")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @Groups({"get_all_restaurants","get_restaurant", "show_after_post_restaurant", "show_restaurant_validated"})
     * @ORM\Column(type="boolean")
     */
    private $validated = false;

    /**
     * @ApiSubresource(
     *     maxDepth=1
     * )
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="restaurants")
     */
    private $users;

    /**
     * @ApiSubresource()
     * @ORM\OneToMany(targetEntity=RestaurantJoinRequest::class, mappedBy="restaurant")
     */
    private $restaurantJoinRequests;

    public function __construct()
    {
        $this->restaurantTypes = new ArrayCollection();
        $this->schedules = new ArrayCollection();
        $this->reservations = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->restaurantJoinRequests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNbPlace(): ?int
    {
        return $this->nbPlace;
    }

    public function setNbPlace(int $nbPlace): self
    {
        $this->nbPlace = $nbPlace;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPhonenumber(): ?string
    {
        return $this->phonenumber;
    }

    public function setPhonenumber(string $phonenumber): self
    {
        $this->phonenumber = $phonenumber;

        return $this;
    }

    /**
     * @return Collection|RestaurantType[]
     */
    public function getRestaurantTypes(): Collection
    {
        return $this->restaurantTypes;
    }

    public function addRestaurantType(RestaurantType $restaurantType): self
    {
        if (!$this->restaurantTypes->contains($restaurantType)) {
            $this->restaurantTypes[] = $restaurantType;
            $restaurantType->addRestaurant($this);
        }

        return $this;
    }

    public function removeRestaurantType(RestaurantType $restaurantType): self
    {
        if ($this->restaurantTypes->removeElement($restaurantType)) {
            $restaurantType->removeRestaurant($this);
        }

        return $this;
    }

    /**
     * @return Collection|Schedule[]
     */
    public function getSchedules(): Collection
    {
        return $this->schedules;
    }

    public function addSchedule(Schedule $schedule): self
    {
        if (!$this->schedules->contains($schedule)) {
            $this->schedules[] = $schedule;
            $schedule->setRestaurant($this);
        }

        return $this;
    }

    public function removeSchedule(Schedule $schedule): self
    {
        if ($this->schedules->removeElement($schedule)) {
            // set the owning side to null (unless already changed)
            if ($schedule->getRestaurant() === $this) {
                $schedule->setRestaurant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Reservation[]
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setRestaurant($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getRestaurant() === $this) {
                $reservation->setRestaurant(null);
            }
        }

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getValidated(): ?bool
    {
        return $this->validated;
    }

    public function setValidated(bool $validated): self
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addRestaurant($this);
        }

        return $this;
    }

    public function hasUser(UserInterface|string $userToFind): bool
    {
        if (!$userToFind instanceof UserInterface)
            return false;

        foreach ($this->getUsers() as $user) {
            if ($user->getId() == $userToFind->getId())
                return true;
        }
        return false;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeRestaurant($this);
        }

        return $this;
    }

    /**
     * @return Collection|RestaurantJoinRequest[]
     */
    public function getRestaurantJoinRequests(): Collection
    {
        return $this->restaurantJoinRequests;
    }

    public function addRestaurantJoinRequest(RestaurantJoinRequest $restaurantJoinRequest): self
    {
        if (!$this->restaurantJoinRequests->contains($restaurantJoinRequest)) {
            $this->restaurantJoinRequests[] = $restaurantJoinRequest;
            $restaurantJoinRequest->setRestaurant($this);
        }

        return $this;
    }

    public function removeRestaurantJoinRequest(RestaurantJoinRequest $restaurantJoinRequest): self
    {
        if ($this->restaurantJoinRequests->removeElement($restaurantJoinRequest)) {
            // set the owning side to null (unless already changed)
            if ($restaurantJoinRequest->getRestaurant() === $this) {
                $restaurantJoinRequest->setRestaurant(null);
            }
        }

        return $this;
    }
}
