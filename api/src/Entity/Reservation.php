<?php

namespace App\Entity;

use App\Repository\ReservationRepository;
use App\Custom\Validator\Constraints as CustomAssert;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 * 		itemOperations={
 * 			"GET"={
 * 				"security"="false",
 * 			},
 * 			"PUT"={
 * 				"security_post_denormalize"="is_granted('PUT', object)",
 * 				"denormalization_context"={"groups"={"put_reservation"}},
 * 				"normalization_context"={"groups"={"show_reservation_after_put"}}
 * 			},
 * 			"DELETE"={
 *              "security"="is_granted('DELETE', object)",
 * 				"openapi_context"={
 *                 "summary"="Delete a reservation",
 *                 "requestBody"={
 *                     "content"={
 *                         "application/ld+json"={
 *                              "schema"={
 *                                  "type"="object",
 *                                  "properties"={
 *                                       "reason"={"type"="string"}
 *                                   }
 *                               }
 *                          }
 *                      }
 *                 }
 *             }
 *          },
 * 		},
 *     subresourceOperations={
 * 			"api_users_reservations_get_subresource"={
 * 				"security"="is_granted('VIEW_AS_USER_SUBRESOURCE', object)",
 * 				"normalization_context"={"groups"={"user_get_reservations"}}
 * 			},
 *          "api_restaurants_reservations_get_subresource"={
 *              "security"="is_granted('VIEW_AS_RESTAURANT_SUBRESOURCE', object)",
 * 				"normalization_context"={"groups"={"restaurant_get_reservations"}}
 *          },
 * 			"api_schedules_reservations_get_subresource"={
 * 				"security"="is_granted('VIEW_AS_SCHEDULE_SUBRESOURCE', object)",
 * 				"normalization_context"={"groups"={"schedule_get_reservations"}}
 * 			},
 *     },
 * 	   collectionOperations={
 * 		"POST"={
 * 				"denormalization_context"={"groups"={"create_reservation"}},
 * 				"normalization_context"={"groups"={"show_reservation_after_post"}},
 * 				"security_post_denormalize"="is_granted('POST', object)",
 * 			}
 * 		}
 * )
 * @ORM\Entity(repositoryClass=ReservationRepository::class)
 */
class Reservation
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 * @Groups({"read_schedule","read_all_schedules","schedule_get_reservations",
	 * 			"restaurant_get_reservations","user_get_reservations",
	 * 			"show_reservation_after_post", "show_reservation_after_put"})
	 */
	private $id;

	/**
	 * @ORM\Column(type="date")
	 * @Groups({"read_schedule","read_all_schedules","schedule_get_reservations",
	 * 			"restaurant_get_reservations","user_get_reservations",
	 * 			"create_reservation", "show_reservation_after_post",
	 * 			"put_reservation", "show_reservation_after_put"})
	 * @CustomAssert\CheckReservationDays(
     *     message = "Il n'est pas possible de réserver à cette date-là"
     * )
	 * @CustomAssert\CheckFutureDate(
	 * 		message = "Vous ne pouvez pas réserver alors que ce service a déjà commencé!"
	 * )
	 */
	private $date;

	/**
	 * @ORM\ManyToOne(targetEntity=User::class, inversedBy="reservations")
	 * @ORM\JoinColumn(nullable=true)
	 * @Groups({"schedule_get_reservations","restaurant_get_reservations",
	 * 			"show_reservation_after_post","show_reservation_after_put"})
	 */
	private $customer;

	/**
	 * @ORM\ManyToOne(targetEntity=Schedule::class, inversedBy="reservations")
	 * @ORM\JoinColumn(nullable=false)
	 * @Groups({"restaurant_get_reservations","user_get_reservations",
	 * 			"show_reservation_after_post","show_reservation_after_put"})
	 */
	private $schedule;

	/**
	 * @ORM\ManyToOne(targetEntity=Restaurant::class, inversedBy="reservations")
	 * @ORM\JoinColumn(nullable=false)
	 * @Groups({"schedule_get_reservations","user_get_reservations",
	 * 			"show_reservation_after_post", "show_reservation_after_put"})
	 */
	private $restaurant;

    /**
     * @ORM\Column(type="integer")
	 * @Groups({"schedule_get_reservations","restaurant_get_reservations","user_get_reservations",
	 * 			"create_reservation","show_reservation_after_post","put_reservation",
	 * 			"show_reservation_after_put"})
	 * @CustomAssert\CheckSeatAvaibility(
     *     message = "Le nombre de personnes est trop élevé pour effectuer la réservation"
     * )
	 * @Assert\Range(
     *      min = 1,
	 * 		max = 5000,
     *      notInRangeMessage = "Une réservation doit prendre entre {{ min }} et {{ max }} personnes",
     * )
     */
    private $nbpersonnes;

    /**
     * @Groups({"create_reservation"})
     */
    private $specifiedCustomerID;

    /**
     * @Groups({"create_reservation","put_reservation"})
     */
    private $scheduleID;

    /**
     * @Assert\Email(
     *     message = "Vous devez rentrer une adresse mail valide"
     * )
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"create_reservation", "schedule_get_reservations","restaurant_get_reservations", "show_reservation_after_post", "show_reservation_after_put"})
     */
    private $emailToConfirm;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
	 * @Groups({"create_reservation","put_reservation", "schedule_get_reservations",
     * 			"restaurant_get_reservations","user_get_reservations",
	 * 			"show_reservation_after_put", "show_reservation_after_post"})
     */
    private $comment;

	public function __construct()
	{
		$this->reservations = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getCustomer(): ?User
	{
		return $this->customer;
	}

	public function setCustomer(?User $customer): self
	{
		$this->customer = $customer;

		return $this;
	}

	public function getRestaurant(): ?Restaurant
	{
		return $this->restaurant;
	}

	public function setRestaurant(?Restaurant $restaurant): self
	{
		$this->restaurant = $restaurant;

		return $this;
	}

	public function getSchedule(): ?Schedule
	{
		return $this->schedule;
	}

	public function setSchedule(?Schedule $schedule): self
	{
		$this->schedule = $schedule;

		return $this;
	}

	public function getDate(): ?\DateTimeInterface
	{
		return $this->date;
	}

	public function setDate(\DateTimeInterface $date): self
	{
		$this->date = $date;

		return $this;
	}

    public function getNbpersonnes(): ?int
    {
        return $this->nbpersonnes;
    }

    public function setNbpersonnes(int $nbpersonnes): self
    {
        $this->nbpersonnes = $nbpersonnes;

        return $this;
    }

    public function getSpecifiedCustomerID(): ?int
    {
        return $this->specifiedCustomerID;
    }

    public function setSpecifiedCustomerID(?int $specifiedCustomerID): self
    {
        $this->specifiedCustomerID = $specifiedCustomerID;

        return $this;
    }

    public function getScheduleID(): ?int
    {
        return $this->scheduleID;
    }

    public function setScheduleID(?int $scheduleID): self
    {
        $this->scheduleID = $scheduleID;

        return $this;
    }

    public function getEmailToConfirm(): ?string
    {
        return $this->emailToConfirm;
    }

    public function setEmailToConfirm(?string $emailToConfirm): self
    {
        $this->emailToConfirm = $emailToConfirm;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }
}
