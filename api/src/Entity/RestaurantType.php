<?php

namespace App\Entity;

use App\Repository\RestaurantTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "GET"={
 *              "normalization_context"={"groups"={"show_restaurant"}}
 *          }
 *     },
 *     itemOperations={
 *          "GET"={
 *              "normalization_context"={"groups"={"show_restaurant"}}
 *          }
 *     },
 *     subresourceOperations={
 *          "api_restaurants_restaurant_types_get_subresource"={
 *              "security"="is_granted('VIEW_AS_RESTAURANT_SUBRESOURCE', object)",
 *              "normalization_context"={"groups"={"show_restaurant"}}
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass=RestaurantTypeRepository::class)
 */
class RestaurantType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @Groups({"get_all_restaurants","get_restaurant","show_after_patch_restaurant_types","show_restaurant"})
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"get_all_restaurants","get_restaurant","show_after_patch_restaurant_types","show_restaurant"})
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=Restaurant::class, inversedBy="restaurantTypes")
     */
    private $restaurants;

    public function __construct()
    {
        $this->restaurants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Restaurant[]
     */
    public function getRestaurants(): Collection
    {
        return $this->restaurants;
    }

    public function addRestaurant(Restaurant $restaurant): self
    {
        if (!$this->restaurants->contains($restaurant)) {
            $this->restaurants[] = $restaurant;
        }

        return $this;
    }

    public function removeRestaurant(Restaurant $restaurant): self
    {
        $this->restaurants->removeElement($restaurant);

        return $this;
    }
}
