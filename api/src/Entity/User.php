<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use App\Custom\Filter\ArrayCheckFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;
use App\Repository\UserRepository;
use App\Service\AccountConfirmationService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\User\ChangePasswordController;
use App\Controller\User\ConfirmAccountController;
use App\Custom\Validator\Constraints as CustomAssert;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Controller\Restaurant\ListRestaurantsForMembersController;
use App\Controller\User\GetMeInformationsController;
use App\Controller\User\FindUserByMailController;
use App\Controller\User\ForgottenPasswordController;

/**
 * @ApiResource(
 *     itemOperations={
 *      "GET"={
 *             "security"="is_granted('ROLE_ADMIN') or object == user",
 *             "normalization_context"={"groups"={"user_get"}}
 *      },
 *      "PATCH"={
 *             "denormalization_context"={"groups"={"user_patch"}},
 *             "normalization_context"={"groups"={"user_show_after_patch"}},
 *             "security"="is_granted('ROLE_ADMIN')",
 *             "openapi_context"={
 *                 "requestBody"={
 *                     "content"={
 *                         "application/ld+json"={
 *                              "schema"={
 *                                  "type"="object",
 *                                  "properties"={
 *                                      "restorerValidated"={"type"="boolean"},
 *                                      "banned"={"type"="boolean"},
 *                                      "reason"={"type"="string"}
 *                                   }
 *                               }
 *                          }
 *                      }
 *                 }
 *             }
 *      },
 *      "PUT"={
 *             "denormalization_context"={"groups"={"user_put"}},
 *             "normalization_context"={"groups"={"user_show_after_put"}},
 *             "security"="object == user"
 *      },
 *      "change_password"={
 *             "method"="PATCH",
 *             "path"="/users/{id}/change_password",
 *             "controller"= ChangePasswordController::class,
 *             "denormalization_context"={"groups"={"reset_password"}},
 *             "normalization_context"={"groups"={"reset_password_show"}},
 *             "security"="object == user"
 *       }
 *     },
 *     collectionOperations={
 *      "GET"={
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "normalization_context"={"groups"={"user_get_all"}}
 *      },
 *     "POST"={
 *              "denormalization_context"={"groups"={"user_create"}},
 *              "normalization_context"={"groups"={"user_show_after_create"}}
 *      },
 *     "find_by_email"={
 *     			"path"="/users/find_by_email",
 * 				"method"="GET",
 *              "security"="is_granted('ROLE_RESTORER')",
 *				"denormalization_context"={"groups"={"user_by_mail"}},
 *              "normalization_context"={"groups"={"show_user_by_mail"}},
 *     			"controller"= FindUserByMailController::class
 *	 	},
 *      "confirm_account"={
 *             "method"="PATCH",
 *             "path"="/users/confirm_account",
 *             "controller"= ConfirmAccountController::class,
 *             "denormalization_context"={"groups"={"confirm_account"}},
 *             "normalization_context"={"groups"={"confirm_account_show"}},
 *       },
 *      "forgotten_password"={
 *             "method"="POST",
 *             "path"="/users/forgotten_password",
 *             "denormalization_context"={"groups"={"forgot_password"}},
 *             "normalization_context"={"groups"={"confirm_account_show"}},
 *             "controller"= ForgottenPasswordController::class
 *       },
 *       "me"={
 *             "method"="GET",
 *             "path"="/me",
 *             "controller"= GetMeInformationsController::class,
 *             "normalization_context"={"groups"={"user_get_me"}},
 *       }
 *     },
 *     subresourceOperations={
 *          "api_restaurants_users_get_subresource"={
 *              "security"="is_granted('VIEW_AS_RESTAURANT_SUBRESOURCE', object)",
 *              "normalization_context"={"groups"={"user_get_all"}}
 *          },
 *          "owned_restaurants_get_subresource"={
 *              "method"="GET",
 *              "controller"= ListRestaurantsForMembersController::class
 *          },
 *          "restaurants_get_subresource"={
 *              "method"="GET",
 *              "controller"= ListRestaurantsForMembersController::class
 *          }
 *     }
 * )
 * @ApiFilter(ArrayCheckFilter::class, properties={"roles"})
 * @ApiFilter(SearchFilter::class, properties={"email"="ipartial","firstname"="ipartial","lastname"="ipartial","restorerValidated"="exact"})
 * @ApiFilter(ExistsFilter::class, properties={"registerToken"})
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"},message="Adresse mail déjà utilisée")
 * @ORM\Table(name="`user`")
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{

    const ROLE_USER = "ROLE_USER";
    const ROLE_ADMIN = "ROLE_ADMIN";
    const ROLE_RESTORER = "ROLE_RESTORER";

    /**
     * @Groups({"user_show_after_create","user_get","user_get_all", "user_get_me", "show_user_by_mail",
     *          "get_restaurant_type_suggestion","get_all_restaurants_type_suggestion",
     *          "get_all_restaurants","get_restaurant","get_restaurant_join_request",
     *          "show_reservation_aftr_post", "schedule_get_reservations","restaurant_get_reservations"})
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @Assert\Email
     * @Assert\Length(
     *     min = 6,
     *     max = 180,
     *     minMessage = "Votre adresse mail doit faire au moins {{ limit }} caractères",
     *     maxMessage = "Votre adresse mail ne peut pas faire plus de {{ limit }} caractères"
     * )
     * @Groups({"user_create","user_show_after_create","user_get", "user_get_me", "show_user_by_mail",
     *          "user_get_all","get_restaurant_type_suggestion",
     *          "get_all_restaurants_type_suggestion","get_restaurant_join_request",
     *          "schedule_get_reservations","restaurant_get_reservations", "forgot_password"})
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @Assert\Length(
     *     min = 2,
     *     max = 20,
     *     minMessage = "Votre prénom doit faire au moins {{ limit }} caractères",
     *     maxMessage = "Votre prénom mail ne peut pas faire plus de {{ limit }} caractères"
     * )
     * @Groups({"user_create","user_show_after_create","user_put","user_show_after_put", "show_user_by_mail",
     *          "user_get","user_get_all", "user_get_me","get_restaurant_type_suggestion",
     *          "get_all_restaurants_type_suggestion","get_restaurant_join_request",
     *          "show_reservation_after_post", "schedule_get_reservations","restaurant_get_reservations"})
     * @ORM\Column(type="string", length=20)
     */
    private $firstname;

    /**
     * @Assert\Length(
     *     min = 2,
     *     max = 20,
     *     minMessage = "Votre nom doit faire au moins {{ limit }} caractères",
     *     maxMessage = "Votre nom ne peut pas faire plus de {{ limit }} caractères"
     * )
     * @Groups({"user_create","user_show_after_create","user_put","user_show_after_put", "show_user_by_mail",
     *          "user_get","user_get_all", "user_get_me","get_restaurant_type_suggestion",
     *          "get_all_restaurants_type_suggestion","get_restaurant_join_request",
     *          "show_reservation_after_post", "schedule_get_reservations","restaurant_get_reservations"})
     * @ORM\Column(type="string", length=20)
     */
    private $lastname;

    /**
     * @var string
     * @Groups({"confirm_account"})
     * @ORM\Column(type="string", nullable=true)
     */
    private $registerToken;


    /**
     * @Groups({"user_show_after_create","user_get","user_get_all", "user_get_me","user_create"})
     * @ORM\Column(type="json")
     */
    private $roles = [User::ROLE_USER];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @Groups({"user_patch","user_show_after_patch","user_get","user_get_all"})
     * @var boolean To check if the restorer is validated by admin
     * @ORM\Column(type="boolean")
     */
    private $restorerValidated = false;

    /**
     * @Groups({"user_patch","user_show_after_patch","user_get","user_get_all","get_restaurant_type_suggestion","get_all_restaurants_type_suggestion"})
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $banned = false;

    /**
     * @CustomAssert\ComplexityPassword(
     *     message = "Votre mot de passe n'est pas assez complexe"
     * )
     * @Groups({"user_create","reset_password"})
     */
    private $plainPassword = "";

    /**
     * @ApiSubresource()
     * @ORM\OneToMany(targetEntity=Reservation::class, mappedBy="customer", orphanRemoval=true)
     */
    private $reservations;

    /**
     * @ORM\OneToMany(targetEntity=RestaurantTypeSuggestion::class, mappedBy="proposer", orphanRemoval=true)
     */
    private $restaurantTypeSuggestions;

    /**
     * @ApiSubresource(
     *     maxDepth=1
     * )
     * @ORM\OneToMany(targetEntity=Restaurant::class, mappedBy="owner", orphanRemoval=true)
     */
    private $ownedRestaurants;

    /**
     * @ApiSubresource(
     *     maxDepth=1
     * )
     * @ORM\ManyToMany(targetEntity=Restaurant::class, inversedBy="users")
     */
    private $restaurants;

    /**
     * @ApiSubresource()
     * @ORM\OneToMany(targetEntity=RestaurantJoinRequest::class, mappedBy="restorer")
     */
    private $restaurantJoinRequests;


    public function __construct()
    {
        $this->reservations = new ArrayCollection();
        $this->restaurants = new ArrayCollection();
        $this->restaurantTypeSuggestions = new ArrayCollection();
        $this->ownedRestaurants = new ArrayCollection();
        $this->registerToken = AccountConfirmationService::generateRandomString();
        $this->restaurantJoinRequests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getRegisterToken(): ?string
    {
        return $this->registerToken;
    }

    public function setRegisterToken($registerToken): self
    {
        $this->registerToken = $registerToken;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPlainPassword(): string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }


    public function getRestorerValidated(): bool {
        return $this->restorerValidated;
    }

    public function setRestorerValidated(bool $restorerValidated): self {
        $this->restorerValidated = $restorerValidated;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Reservation[]
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setCustomer($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getCustomer() === $this) {
                $reservation->setCustomer(null);
            }
        }

        return $this;
    }

    public function getBanned(): ?bool
    {
        return $this->banned;
    }

    public function setBanned(bool $banned): self
    {
        $this->banned = $banned;

        return $this;
    }

    /**
     * @return Collection|RestaurantTypeSuggestion[]
     */
    public function getRestaurantTypeSuggestions(): Collection
    {
        return $this->restaurantTypeSuggestions;
    }

    public function addRestaurantTypeSuggestion(RestaurantTypeSuggestion $restaurantTypeSuggestion): self
    {
        if (!$this->restaurantTypeSuggestions->contains($restaurantTypeSuggestion)) {
            $this->restaurantTypeSuggestions[] = $restaurantTypeSuggestion;
            $restaurantTypeSuggestion->setProposer($this);
        }

        return $this;
    }

    public function removeRestaurantTypeSuggestion(RestaurantTypeSuggestion $restaurantTypeSuggestion): self
    {
        if ($this->restaurantTypeSuggestions->removeElement($restaurantTypeSuggestion)) {
            // set the owning side to null (unless already changed)
            if ($restaurantTypeSuggestion->getProposer() === $this) {
                $restaurantTypeSuggestion->setProposer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Restaurant[]
     */
    public function getOwnedRestaurants(): Collection
    {
        return $this->ownedRestaurants;
    }

    public function addOwnedRestaurant(Restaurant $ownedRestaurant): self
    {
        if (!$this->ownedRestaurants->contains($ownedRestaurant)) {
            $this->ownedRestaurants[] = $ownedRestaurant;
            $ownedRestaurant->setOwner($this);
        }

        return $this;
    }

    public function removeOwnedRestaurant(Restaurant $ownedRestaurant): self
    {
        if ($this->ownedRestaurants->removeElement($ownedRestaurant)) {
            // set the owning side to null (unless already changed)
            if ($ownedRestaurant->getOwner() === $this) {
                $ownedRestaurant->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Restaurant[]
     */
    public function getRestaurants(): Collection
    {
        return $this->restaurants;
    }

    public function addRestaurant(Restaurant $restaurant): self
    {
        if (!$this->restaurants->contains($restaurant)) {
            $this->restaurants[] = $restaurant;
        }

        return $this;
    }

    public function removeRestaurant(Restaurant $restaurant): self
    {
        $this->restaurants->removeElement($restaurant);

        return $this;
    }

    /**
     * @return Collection|RestaurantJoinRequest[]
     */
    public function getRestaurantJoinRequests(): Collection
    {
        return $this->restaurantJoinRequests;
    }

    public function addRestaurantJoinRequest(RestaurantJoinRequest $restaurantJoinRequest): self
    {
        if (!$this->restaurantJoinRequests->contains($restaurantJoinRequest)) {
            $this->restaurantJoinRequests[] = $restaurantJoinRequest;
            $restaurantJoinRequest->setRestorer($this);
        }

        return $this;
    }

    public function removeRestaurantJoinRequest(RestaurantJoinRequest $restaurantJoinRequest): self
    {
        if ($this->restaurantJoinRequests->removeElement($restaurantJoinRequest)) {
            // set the owning side to null (unless already changed)
            if ($restaurantJoinRequest->getRestorer() === $this) {
                $restaurantJoinRequest->setRestorer(null);
            }
        }

        return $this;
    }
}
