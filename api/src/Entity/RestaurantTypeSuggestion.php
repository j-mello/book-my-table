<?php

namespace App\Entity;

use App\Repository\RestaurantTypeSuggestionRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\RestaurantTypeSuggestion\ProposeNewTypeController;
use App\Controller\RestaurantTypeSuggestion\AcceptTypeSuggestionController;
use App\Controller\RestaurantTypeSuggestion\DenyTypeSuggestionController;

/**
 * @ApiResource(
 *     itemOperations={
 *       "GET"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "normalization_context"={"groups"={"get_restaurant_type_suggestion"}}
 *        },
 *       "accept"={
 *          "method"="POST",
 *          "path"="/restaurant_type_suggestions/{id}/accept",
 *          "controller"= AcceptTypeSuggestionController::class,
 *          "denormalization_context"={"groups"={"accept_suggestion_type"}},
 *          "normalization_context"={"groups"={"accept_suggestion_type"}},
 *          "security"="is_granted('ROLE_ADMIN')"
 *        },
 *       "deny"={
 *          "method"="POST",
 *          "path"="/restaurant_type_suggestions/{id}/deny",
 *          "controller"= DenyTypeSuggestionController::class,
 *          "denormalization_context"={"groups"={"deny_suggestion_type"}},
 *          "normalization_context"={"groups"={"deny_suggestion_type"}},
 *          "security"="is_granted('ROLE_ADMIN')"
 *        }
 *     },
 *     collectionOperations={
 *        "GET"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "normalization_context"={"groups"={"get_all_restaurants_type_suggestion"}}
 *        },
 *        "propose_new"={
 *          "method"="POST",
 *          "path"="/restaurant_type_suggestions/propose_new",
 *          "controller"= ProposeNewTypeController::class,
 *          "denormalization_context"={"groups"={"propose_new_restaurant_type"}},
 *          "normalization_context"={"groups"={"propose_new_restaurant_type_show"}},
 *          "security"="is_granted('ROLE_RESTORER')"
 *        }
 *     }
 * )
 * @ORM\Entity(repositoryClass=RestaurantTypeSuggestionRepository::class)
 */
class RestaurantTypeSuggestion
{
    /**
     * @Groups({"get_restaurant_type_suggestion","get_all_restaurants_type_suggestion", "propose_new_restaurant_type_show"})
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"get_restaurant_type_suggestion","get_all_restaurants_type_suggestion", "propose_new_restaurant_type", "propose_new_restaurant_type_show"})
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Groups({"get_restaurant_type_suggestion","get_all_restaurants_type_suggestion"})
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="restaurantTypeSuggestions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $proposer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getProposer(): ?User
    {
        return $this->proposer;
    }

    public function setProposer(?User $proposer): self
    {
        $this->proposer = $proposer;

        return $this;
    }
}
