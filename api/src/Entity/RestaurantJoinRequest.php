<?php

namespace App\Entity;

use App\Repository\RestaurantJoinRequestRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\Restaurant\AcceptRestaurantJoinRequestController;
use App\Controller\Restaurant\DenyRestaurantJoinRequestController;

/**
 * @ApiResource(
 *     itemOperations={
 *         "GET"={
 *              "normalization_context"={"groups"={"get_restaurant_join_request"}},
 *              "security"="is_granted('ROLE_RESTORER') && (user == object.getRestaurant().getOwner() || user == object.getRestorer())"
 *          },
 *          "accept_request"={
 *              "method"="POST",
 *              "path"="/restaurant_join_requests/{id}/accept",
 *              "controller"= AcceptRestaurantJoinRequestController::class,
 *              "security"="is_granted('ROLE_RESTORER') && user == object.getRestaurant().getOwner()",
 *              "normalization_context"={"groups"={"accept_restaurant_join_request"}},
 *              "denormalization_context"={"groups"={"accept_restaurant_join_request"}}
 *          },
 *          "deny_request"={
 *              "method"="POST",
 *              "path"="/restaurant_join_requests/{id}/deny",
 *              "controller"= DenyRestaurantJoinRequestController::class,
 *              "security"="is_granted('ROLE_RESTORER') && user == object.getRestaurant().getOwner()",
 *              "normalization_context"={"groups"={"deny_restaurant_join_request"}},
 *              "denormalization_context"={"groups"={"deny_restaurant_join_request"}}
 *          }
 *     },
 *     collectionOperations={
 *          "GET"={
 *              "security"="false"
 *          }
 *     },
 *     subresourceOperations={
 *          "api_restaurants_restaurant_join_requests_get_subresource"={
 *              "normalization_context"={"groups"={"get_restaurant_join_request"}},
 *              "security"="is_granted('VIEW_AS_RESTAURANT_SUBRESOURCE', object)"
 *          },
 *          "api_users_restaurant_join_requests_get_subresource"={
 *              "normalization_context"={"groups"={"get_restaurant_join_request"}},
 *              "security"="is_granted('VIEW_AS_USER_SUBRESOURCE', object)"
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass=RestaurantJoinRequestRepository::class)
 */
class RestaurantJoinRequest
{
    /**
     * @Groups("get_restaurant_join_request")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("get_restaurant_join_request")
     * @ORM\ManyToOne(targetEntity=Restaurant::class, inversedBy="restaurantJoinRequests")
     */
    private $restaurant;

    /**
     * @Groups("get_restaurant_join_request")
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="restaurantJoinRequests")
     */
    private $restorer;

    /**
     * @Groups("get_restaurant_join_request")
     * @ORM\Column(type="string", length=255)
     */
    private $reason;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    }

    public function setRestaurant(?Restaurant $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    public function getRestorer(): ?User
    {
        return $this->restorer;
    }

    public function setRestorer(?User $restorer): self
    {
        $this->restorer = $restorer;

        return $this;
    }

    public function getReason(): ?string
    {
        return $this->reason;
    }

    public function setReason(string $reason): self
    {
        $this->reason = $reason;

        return $this;
    }
}
