<?php

namespace App\Entity;

use App\Repository\ScheduleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Custom\Validator\Constraints as CustomAssert;

/**
 * @ApiResource(
 *      normalizationContext={"groups"={"read_schedule"}},
 *      itemOperations={
 *          "GET"={
 *              "normalization_context"={"groups"={"read_schedule"}},
 *              "security"="is_granted('GET', object)"
 *          },
 *          "PUT"={
 *              "security"="is_granted('PUT', object)",
 *              "denormalization_context"={"groups"={"update_schedule"}},
 *              "normalization_context"={"groups"={"update_schedule"}},
 *              "validation_groups"={"Default", "update_validation"}
 *          },
 *          "DELETE"={
 *              "denormalization_context"={"groups"={"delete_schedule"}},
 *              "security"="is_granted('DELETE', object)"
 *          },
 *      },
 *      collectionOperations={
 *           "POST"={
 *              "method"="POST",
 *              "security_post_denormalize"="is_granted('POST', object)",
 *              "denormalization_context"={"groups"={"create_schedule"}},
 *              "normalization_context"={"groups"={"create_schedule"}},
 *              "validation_groups"={"Default", "create_validation"}
 *            }
 *      },
 *     subresourceOperations={
 *          "api_restaurants_schedules_get_subresource"={
 *              "normalization_context"={"groups"={"read_all_schedules"}},
 *              "security"="is_granted('VIEW_AS_RESTAURANT_SUBRESOURCE', object)"
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass=ScheduleRepository::class)
 */
class Schedule
{

	const MONDAY = 0;
	const TUESDAY = 1;
	const WEDNESDAY = 2;
	const THURSDAY = 3;
	const FRIDAY = 4;
	const SATURDAY = 5;
	const SUNDAY = 6;

	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 * @Groups({"update_schedule","create_schedule","read_schedule",
	 * 			"read_all_schedules","show_reservation_after_post", "show_reservation_after_put", "restaurant_get_reservations","user_get_reservations"})
	 */
	private $id;

	/**
     * @CustomAssert\RestaurantExists(message = "Ce restaurant n'existe pas")
	 * @Groups({"create_schedule"})
	 */
	private $restaurantId;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 * @Groups({"read_all_schedules", "read_schedule", "create_schedule",
	 * 			"update_schedule","show_reservation_after_post",
	 * 			"show_reservation_after_put", "restaurant_get_reservations","user_get_reservations"})
	 * @Assert\NotBlank(groups={"create_validation"})
	 * @Assert\Length(
	 *     min = 3,
	 *     max = 50,
     *     minMessage = "Le nom doit faire au moins {{ limit }} caractères",
     *     maxMessage = "Le nom ne peut pas faire plus de {{ limit }} caractères",
	 *     groups={"create_validation", "update_validation"}
	 * )
	 */
	private $name;

	/**
	 * @ORM\Column(type="time")
	 * @Groups({"read_all_schedules", "read_schedule", "create_schedule", "update_schedule","show_reservation_after_post",
     * 			"show_reservation_after_put", "restaurant_get_reservations","user_get_reservations"})
	 * @Assert\NotBlank(groups={"create_validation", "update_validation"})
	 */
	private $start;

	/**
	 * @ORM\Column(type="time")
	 * @Groups({"read_all_schedules", "read_schedule", "create_schedule", "update_schedule", "show_reservation_after_post",
     * 			"show_reservation_after_put", "restaurant_get_reservations","user_get_reservations"})
	 * @Assert\NotBlank(groups={"create_validation", "update_validation"})
	 */
	private $ending;

	/**
	 * @ORM\Column(type="integer")
	 * @Groups({"read_all_schedules", "read_schedule", "create_schedule","update_schedule"})
     * @Assert\Range(
     *      min = 4,
     *      max = 5000,
     *      notInRangeMessage = "Un service doit avoir entre {{ min }} et {{ max }} places",
     *      groups={"create_validation", "update_validation"}
     * )
     * @CustomAssert\NbPlaceNotExceedRestaurantNbPlace(
     *     message = "Vous ne pouvez pas dépasser le nombre de places du restaurant",
     *     groups={"create_validation", "update_validation"}
     * )
     * @CustomAssert\NbPlaceDontConflictWithReservations(
     *     message = "Vous ne pouvez pas définir le nombre places à {{ nbPlace }}, elles sont prises par des réservations",
     *     groups={"update_validation"}
     * )
	 */
	private $nbPlace;

	/**
	 * @ORM\ManyToOne(targetEntity=Restaurant::class, inversedBy="schedules")
	 * @ORM\JoinColumn(nullable=false)
	 * @Groups({"read_schedule"})
	 */
	private $restaurant;

	/**
	 * @ORM\OneToMany(targetEntity=Reservation::class, mappedBy="schedule", orphanRemoval=true)
	 * @Groups({"read_schedule","read_all_schedules"})
	 * @ApiSubresource()
	 */
	private $reservations;

	/**
	 * @ORM\Column(type="json")
     * @CustomAssert\DaysMentioned(
     *     message = "Vous devez mentionner des jours",
     *     groups={"create_validation", "update_validation"}
     * )
     * @CustomAssert\DaysValid(
     *     message = "Vous avez rentré des jours invalides",
     *     groups={"create_validation", "update_validation"}
     * )
     * @CustomAssert\DaysDontConflictWithReservations(
     *     message = "Vous ne pouvez pas changer les jours de ce service, car des reservations y ont déjà été prises",
     *     groups={"update_validation"}
     * )
	 * @Groups({"read_all_schedules","read_schedule", "create_schedule", "update_schedule", "get_restaurant"})
	 */
	private $days = [Schedule::MONDAY, Schedule::TUESDAY, Schedule::WEDNESDAY, Schedule::THURSDAY, Schedule::FRIDAY];

	public function __construct()
	{
		$this->reservations = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(?string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getStart(): ?\DateTimeInterface
	{
		return $this->start;
	}

	public function setStart(\DateTimeInterface $start): self
	{
		$this->start = $start;

		return $this;
	}

	public function getEnding(): ?\DateTimeInterface
	{
		return $this->ending;
	}

	public function setEnding(\DateTimeInterface $ending): self
	{
		$this->ending = $ending;

		return $this;
	}

	public function getRestaurant(): ?Restaurant
	{
		return $this->restaurant;
	}

	public function setRestaurant(?Restaurant $restaurant): self
	{
		$this->restaurant = $restaurant;
		$this->restaurantId = $restaurant->getId();

		return $this;
	}

	public function getRestaurantId(): ?int
	{
		return $this->restaurantId;
	}

	public function setRestaurantId(?int $restaurantId): self
	{
		$this->restaurantId = $restaurantId;

		return $this;
	}

	/**
	 * @return Collection|Reservation[]
	 */
	public function getReservations(): Collection
	{
		return $this->reservations;
	}

	public function addReservation(Reservation $reservation): self
	{
		if (!$this->reservations->contains($reservation)) {
			$this->reservations[] = $reservation;
			$reservation->setSchedule($this);
		}

		return $this;
	}

	public function deleteReservation(Reservation $reservation): self
	{
		if ($this->reservations->deleteElement($reservation)) {
			// set the owning side to null (unless alread_scheduley changed)
			if ($reservation->getSchedule() === $this) {
				$reservation->setSchedule(null);
			}
		}

		return $this;
	}

	public function getNbPlace(): ?int
	{
		return $this->nbPlace;
	}

	public function setNbPlace(int $nbPlace): self
	{
		$this->nbPlace = $nbPlace;

		return $this;
	}

	public function getDays(): array
	{
		return $this->days;
	}

	public function setDays(array $days): self
	{
		$this->days = $days;

		return $this;
	}
}
