<?php
namespace App\Custom\Validator\Constraints;

use Symfony\Component\Validator\Constraint,
    Symfony\Component\Validator\ConstraintValidator;

class DaysMentionedValidator extends ConstraintValidator {

    public function validate($days, Constraint $constraint) {
        if(count($days) == 0) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }

}
