<?php
namespace App\Custom\Validator\Constraints;

use App\Entity\Restaurant;
use App\Repository\RestaurantRepository;
use App\Service\ScheduleCheckerService;
use Symfony\Component\Validator\Constraint,
    Symfony\Component\Validator\ConstraintValidator;

class NbPlaceNotExceedRestaurantNbPlaceValidator extends ConstraintValidator {

    private $restaurantRepository;

    public function __construct(RestaurantRepository $restaurantRepository) {
        $this->restaurantRepository = $restaurantRepository;
    }

    public function validate($nbPlace, Constraint $constraint) {
        $restaurant = $this->context->getObject()->getRestaurant();
        if (!($restaurant instanceof Restaurant)) {
            $restaurant = $this->restaurantRepository->find($this->context->getObject()->getRestaurantId());
        }
        if($nbPlace > $restaurant->getNbPlace()) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }

}
