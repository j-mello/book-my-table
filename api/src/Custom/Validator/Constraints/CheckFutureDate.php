<?php

namespace App\Custom\Validator\Constraints;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class CheckFutureDate extends Constraint
{
    public $message = 'It is not possible to make a reservervation in the past !';

    public function validatedBy() {
        return get_class($this) . 'Validator';
    }
}