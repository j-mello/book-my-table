<?php
namespace App\Custom\Validator\Constraints;

use App\Service\ScheduleCheckerService;
use Symfony\Component\Validator\Constraint,
    Symfony\Component\Validator\ConstraintValidator;

class DaysValidValidator extends ConstraintValidator {

    private $scheduleCheckerService;

    public function __construct(ScheduleCheckerService $scheduleCheckerService) {
        $this->scheduleCheckerService = $scheduleCheckerService;
    }

    public function validate($days, Constraint $constraint) {
        if(!$this->scheduleCheckerService->checkScheduleDays($days)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }

}
