<?php
namespace App\Custom\Validator\Constraints;

use App\Service\ScheduleCheckerService;
use Symfony\Component\Validator\Constraint,
    Symfony\Component\Validator\ConstraintValidator;

class DaysDontConflictWithReservationsValidator extends ConstraintValidator {

    private $scheduleCheckerService;

    public function __construct(ScheduleCheckerService $scheduleCheckerService) {
        $this->scheduleCheckerService = $scheduleCheckerService;
    }

    public function validate($days, Constraint $constraint) {
        if(!$this->scheduleCheckerService->checkScheduleReservationsDays($this->context->getObject())) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }

}
