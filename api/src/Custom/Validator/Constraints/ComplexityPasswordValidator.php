<?php
namespace App\Custom\Validator\Constraints;

use App\Service\AccountConfirmationService;
use Symfony\Component\Validator\Constraint,
    Symfony\Component\Validator\ConstraintValidator;

class ComplexityPasswordValidator extends ConstraintValidator {
    private $accountConfirmationService;

    public function __construct(AccountConfirmationService $accountConfirmationService) {
        $this->accountConfirmationService = $accountConfirmationService;
    }

    public function validate($plainPassword, Constraint $constraint) {
        if (empty($plainPassword)) return;

        if (!$this->accountConfirmationService->checkPasswordComplexity($plainPassword)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }

}
