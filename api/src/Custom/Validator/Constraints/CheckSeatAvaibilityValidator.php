<?php
namespace App\Custom\Validator\Constraints;

use Symfony\Component\Validator\Constraint,
    Symfony\Component\Validator\ConstraintValidator;
use App\Service\ScheduleCheckerService;
use App\Repository\ScheduleRepository;

class CheckSeatAvaibilityValidator extends ConstraintValidator {

    private $scheduleCheckerService;
    private $scheduleRepository;
    
    public function __construct(ScheduleCheckerService $scheduleCheckerService, ScheduleRepository $scheduleRepository){
        $this->scheduleCheckerService = $scheduleCheckerService;
        $this->scheduleRepository = $scheduleRepository;
    }

    public function validate($nbPersonnes, Constraint $constraint) {
        $reservation = $this->context->getObject();

        $schedule = $reservation->getSchedule();
        
        if($schedule === null)
        {
            $schedule = $this->scheduleRepository->find($reservation->getScheduleID());
        }
        
        if (!$this->scheduleCheckerService->checkOneScheduleReservationNbPlace($schedule, $reservation))
        {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }

}