<?php

namespace App\Custom\Validator\Constraints;
use Symfony\Component\Validator\Constraint;


/**
 * @Annotation
 */
class CheckSeatAvaibility extends Constraint
{
    public $message = 'It is not possible to book for this amount of people';

    public function validatedBy() {
        return get_class($this) . 'Validator';
    }
}