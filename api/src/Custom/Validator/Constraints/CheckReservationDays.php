<?php

namespace App\Custom\Validator\Constraints;
use Symfony\Component\Validator\Constraint;


/**
 * @Annotation
 */
class CheckReservationDays extends Constraint
{
    public $message = 'It is not possible to make a reservervation for this schedule';

    public function validatedBy() {
        return get_class($this) . 'Validator';
    }
}
