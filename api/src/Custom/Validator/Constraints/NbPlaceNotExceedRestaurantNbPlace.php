<?php

namespace App\Custom\Validator\Constraints;
use Symfony\Component\Validator\Constraint;


/**
 * @Annotation
 */
class NbPlaceNotExceedRestaurantNbPlace extends Constraint
{
    public $message = 'Exceed restaurant nbPlace';

    public function validatedBy() {
        return get_class($this) . 'Validator';
    }
}
