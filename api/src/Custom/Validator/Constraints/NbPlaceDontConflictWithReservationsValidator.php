<?php
namespace App\Custom\Validator\Constraints;

use App\Service\ScheduleCheckerService;
use Symfony\Component\Validator\Constraint,
    Symfony\Component\Validator\ConstraintValidator;

class NbPlaceDontConflictWithReservationsValidator extends ConstraintValidator {

    private $scheduleCheckerService;

    public function __construct(ScheduleCheckerService $scheduleCheckerService) {
        $this->scheduleCheckerService = $scheduleCheckerService;
    }

    public function validate($nbPlace, Constraint $constraint) {
        if(!$this->scheduleCheckerService->checkScheduleNbPlace($this->context->getObject())) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ nbPlace }}', $nbPlace)
                ->addViolation();
        }
    }

}
