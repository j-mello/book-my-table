<?php
namespace App\Custom\Validator\Constraints;

use App\Entity\Reservation;
use App\Repository\ScheduleRepository;
use Symfony\Component\Validator\Constraint,
    Symfony\Component\Validator\ConstraintValidator;


class CheckFutureDateValidator extends ConstraintValidator {
	private $scheduleRepository;

	public function __construct(ScheduleRepository $scheduleRepository) {
		$this->scheduleRepository = $scheduleRepository;
	}

    public function validate($date, Constraint $constraint) {
    	$reservation = $this->context->getObject();
    	if (!($reservation instanceof Reservation) || !($date instanceof \DateTimeInterface))
    		return;
    	$schedule = $reservation->getSchedule();
    	if ($schedule === null && $reservation->getScheduleID() !== null) {
    		$schedule = $this->scheduleRepository->find($reservation->getScheduleID());
		}
    	if ($schedule === null)
    		return;

		$reservationMinDateAndTime = new \DateTime($date->format("Y-m-d")." ".$schedule->getStart()->format("H:i"));
    	$now = new \DateTime("now");
    	if ($now > $reservationMinDateAndTime)
    	{
    		$this->context->buildViolation($constraint->message)
				->addViolation();
    	}
    }

}
