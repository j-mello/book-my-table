<?php

namespace App\Custom\Validator\Constraints;
use Symfony\Component\Validator\Constraint;


/**
 * @Annotation
 */
class DaysDontConflictWithReservations extends Constraint
{
    public $message = 'Days are in conflict with reservations';

    public function validatedBy() {
        return get_class($this) . 'Validator';
    }
}
