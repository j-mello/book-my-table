<?php
namespace App\Custom\Validator\Constraints;

use App\Service\DateService;
use Symfony\Component\Validator\Constraint,
    Symfony\Component\Validator\ConstraintValidator;
use App\Service\ScheduleCheckerService;
use App\Repository\ScheduleRepository;

class CheckReservationDaysValidator extends ConstraintValidator {

    private $scheduleCheckerService;
    private $scheduleRepository;
    private $dateService;
    
    public function __construct(ScheduleCheckerService $scheduleCheckerService, ScheduleRepository $scheduleRepository, DateService $dateService){
        $this->scheduleCheckerService = $scheduleCheckerService;
        $this->scheduleRepository = $scheduleRepository;
        $this->dateService = $dateService;
    }

    public function validate($date, Constraint $constraint) {
        
        $reservation = $this->context->getObject();

        $schedule = $reservation->getSchedule();
        
        if($schedule === null)
        {
            $schedule = $this->scheduleRepository->find($reservation->getScheduleID());
        }

        if (!$this->scheduleCheckerService->checkOneScheduleReservationDays($schedule, $date))
        {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }

}