<?php
namespace App\Custom\Validator\Constraints;

use App\Repository\RestaurantRepository;
use Symfony\Component\Validator\Constraint,
    Symfony\Component\Validator\ConstraintValidator;

class RestaurantExistsValidator extends ConstraintValidator {

    private $restaurantRepository;

    public function __construct(RestaurantRepository $restaurantRepository) {
        $this->restaurantRepository = $restaurantRepository;
    }

    public function validate($restaurantId, Constraint $constraint) {
        if ($restaurantId === null) {
            return;
        }
        $restaurant = $this->restaurantRepository->find($restaurantId);
        if($restaurant === null) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }

}
