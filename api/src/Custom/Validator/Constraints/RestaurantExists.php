<?php

namespace App\Custom\Validator\Constraints;
use Symfony\Component\Validator\Constraint;


/**
 * @Annotation
 */
class RestaurantExists extends Constraint
{
    public $message = 'This restaurant does not exist';

    public function validatedBy() {
        return get_class($this) . 'Validator';
    }
}
