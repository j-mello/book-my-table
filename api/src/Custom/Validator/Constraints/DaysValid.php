<?php

namespace App\Custom\Validator\Constraints;
use Symfony\Component\Validator\Constraint;


/**
 * @Annotation
 */
class DaysValid extends Constraint
{
    public $message = 'You need to mention days';

    public function validatedBy() {
        return get_class($this) . 'Validator';
    }
}
