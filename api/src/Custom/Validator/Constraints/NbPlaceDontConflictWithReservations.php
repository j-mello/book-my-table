<?php

namespace App\Custom\Validator\Constraints;
use Symfony\Component\Validator\Constraint;


/**
 * @Annotation
 */
class NbPlaceDontConflictWithReservations extends Constraint
{
    public $message = '{{ nbPlace }} places conflict with reservations';

    public function validatedBy() {
        return get_class($this) . 'Validator';
    }
}
