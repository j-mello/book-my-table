<?php


namespace App\Custom\Filter;


use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;

class ArrayCheckFilter extends AbstractFilter
{

    public function getDescription(string $resourceClass): array
    {
        $description = [];
        foreach ($this->properties as $property => $_) {
            $description[$property] = [
                'property' => null,
                'type' => 'string',
                'required' => false,
                'openapi' => [
                    'description' => 'Filter to apply on an array'
                ]
            ];
        }
        return $description;
    }

    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
        if (array_key_exists($property,$this->properties)) {
            $alias = $queryBuilder->getRootAliases()[0];
            $queryBuilder
                ->andWhere(sprintf("check_arr_contains_element(%s.%s,:value)=true",$alias,$property))
                ->setParameter('value', $value)
            ;
        }
    }
}
