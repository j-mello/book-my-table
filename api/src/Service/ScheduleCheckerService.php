<?php


namespace App\Service;


use App\Entity\Reservation;
use App\Entity\Schedule;

class ScheduleCheckerService {
    private $dateService;

    public function __construct(DateService $dateService) {
        $this->dateService = $dateService;
    }

	public function checkScheduleNbPlace(Schedule $schedule): bool {
		$nbPlacesByDate = [];

		foreach ($schedule->getReservations() as $reservation) {
			$date = $reservation->getDate()->format("Y-m-d");
            $nbPlacesByDate[$date] = isset($nbReservationsByDate[$date]) ? $nbReservationsByDate[$date]+$reservation->getNbpersonnes() : $reservation->getNbpersonnes();
			if ($nbPlacesByDate[$date] > $schedule->getNbPlace())
				return false;
		}
		return true;
	}

	public function checkOneScheduleReservationNbPlace(Schedule $schedule, Reservation $reservation) {
        $gotNbPlaces = 0;
        $date = $reservation->getDate()->format("Y-m-d");
        foreach ($schedule->getReservations() as $eachReservation) {
            if ($eachReservation->getDate()->format("Y-m-d") === $date) {
                $gotNbPlaces += $eachReservation->getNbpersonnes();
            }
        }
        return $gotNbPlaces+$reservation->getNbpersonnes() <= $schedule->getNbPlace();
    }

	public function checkScheduleDays(Array $days): bool {
        foreach ($days as $day) {
            if (!in_array($day, [Schedule::MONDAY,Schedule::TUESDAY,Schedule::WEDNESDAY,Schedule::THURSDAY,Schedule::FRIDAY,Schedule::SATURDAY,Schedule::SUNDAY]))
                return false;
        }
        return true;
    }

	public function checkScheduleReservationsDays(Schedule $schedule): bool {
	    foreach ($schedule->getReservations() as $reservation) {
            if (!$this->checkOneScheduleReservationDays($schedule,$reservation->getDate()))
                return false;
        }
	    return true;
    }

    public function checkOneScheduleReservationDays(Schedule $schedule, \DateTime $date) {
        return in_array($this->dateService->getWeekDayFromDate($date),$schedule->getDays());
    }

	public function checkScheduleForDelete(Schedule $schedule): bool {
		return count($schedule->getReservations()) == 0;
	}
}
