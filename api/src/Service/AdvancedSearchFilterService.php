<?php


namespace App\Service;


use App\Entity\User;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use ReflectionClass;
use Symfony\Component\Config\Definition\Exception\Exception;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Security;

class AdvancedSearchFilterService
{
    private $security;
    private $request;

    public function __construct(Security $security, RequestStack $requestStack) {
        $this->security = $security;
        $this->request = $requestStack->getCurrentRequest();
    }

    private function isGranted($role): bool {
        $user = $this->security->getUser();
        return $user !== null && in_array($role, $user->getRoles());
    }

    private function getFieldType($field, $entityInstance, $modelColumn = null) {
        $docReader = new AnnotationReader();
        $reflect = new ReflectionClass($entityInstance);

        $infos = $docReader->getPropertyAnnotations($reflect->getProperty($field));
        foreach ($infos as $info) {
            if ($info instanceof ORM\Column && is_null($modelColumn)) {
                return $info->type;
            }
            if ($info instanceof ORM\OneToMany ||
                $info instanceof ORM\ManyToOne ||
                $info instanceof ORM\OneToOne ||
                $info instanceof ORM\ManyToMany) {
                if (is_null($modelColumn)) {
                    return "relation";
                }
                $targetEntity = $info->targetEntity;
                return $this->getFieldType($modelColumn,new $targetEntity());
            }
        }
        return null;
    }

    private function convertType($value, $type) {
        if ($type === 'boolean') {
            return $value === 'true' ? true : ($value === 'false' ? false : null);
        }
        if ($type === 'integer') {
            return (int)$value;
        }
        return $value;
    }

    public function checkFilters($filters, $entityName) {
        $entityInstance = new $entityName();
        $user = $this->security->getUser();
        $datas = [];
        foreach ($filters as $filterName => $config) {
            $default = null;
            if (isset($config['default'])) {
                $defaultSetted = false;
                if (!is_array($config['default'])) {
                    $defaultSetted = true;
                    $default = $config['default'];
                }
                if (!$defaultSetted && $user !== null) {
                    foreach ($user->getRoles() as $role) {
                        if (array_key_exists($role,$config['default'])) {
                            $defaultSetted = true;
                            $default = $config['default'][$role];
                            break;
                        }
                    }
                }
                if (!$defaultSetted)  {
                    $default = $config['default']['other']??null;
                }
            }
            $data = $this->request->get($filterName);

            if (array_key_exists('modifiable', $config) && !$config['modifiable'] && !is_null($data)) {
                throw new AccessDeniedHttpException("Vous n'avez pas le droit d'utiliser ce filtre");
            }

            if ($config['type'] == "enum" && !is_null($data)) {
                $data = array_map(function ($elem) {
                    return trim($elem);
                }, explode(",",$data));
            }
            $type = $this->getFieldType($filterName,$entityInstance);
            if ($type == "relation" && !array_key_exists('modelColumn',$config)) {
                throw new Exception("Vous devez spécifier modelColumn quand il s'agit d'une relation");
            }
            $columnType = $type;
            if ($type == "relation") {
                $columnType = $this->getFieldType($filterName,$entityInstance,$config['modelColumn']);
            }
            if (!is_null($data) && !is_null($columnType)) {
                $data = is_array($data) ?
                    array_map(function ($elem) use ($columnType) {
                        return $this->convertType($elem, $columnType);
                    }, $data) : $this->convertType($data, $columnType);
            }
            if (is_null($data)) {
                $data = $default;
            }

            if (isset($config['role']) &&
                !$this->isGranted($config['role']) &&
                $data !== $default) {

                throw new AccessDeniedHttpException("Vous n'avez pas le droit d'utiliser ce filtre");
            }
            $datas[$filterName] = [$data,$config['type'],$type];
            if ($type == "relation") {
                $datas[$filterName][] = $config['modelColumn'];
            } else {
                $datas[$filterName][] = null;
            }
        }
        return $datas;
    }

    public function applyFilters(QueryBuilder $queryBuilder, $datas, $paginate = null): array {
        $alias = $queryBuilder->getRootAliases()[0];
        foreach ($datas as $field => $data) {
            [$value,$type,$typeInDB,$modelColumn] = $data;
            if (!is_null($value)) {
                $queryMethod = "query".ucfirst($type);
                if ($type == "enum") {
                    if ($typeInDB == "relation") {
                        $queryBuilder
                            ->innerJoin("$alias.$field", "_$field", Expr\Join::WITH, $this->queryEnum("_$field.$modelColumn", $field, array_keys($value)));
                    } else {
                        $queryBuilder
                            ->andWhere($this->queryEnum("$alias.$field", $field, array_keys($value)));
                    }
                    foreach ($value as $k => $v) {
                        $queryBuilder->setParameter($field.$k, $v);
                    }
                } else {
                    if ($typeInDB == "relation") {
                        $queryBuilder
                            ->innerJoin("$alias.$field", "_$field", Expr\Join::WITH, $this->$queryMethod("_$field.$modelColumn", $field));
                    } else {
                        $queryBuilder = $queryBuilder
                            ->andWhere($this->$queryMethod("$alias.$field",$field));
                    }
                    $queryBuilder->setParameter($field, in_array($type, ['partial','ipartial']) ? "%".$value."%" : $value);
                }
            }
        }
        if ($paginate !== null) {
            $total = (clone $queryBuilder)
                ->select('count('.$alias.'.id)')
                ->getQuery()
                ->getSingleScalarResult();

            $page = $this->request->get("page");
            $page = $page ? (int)$page : 1;
            $queryBuilder
                ->setMaxResults($paginate)
                ->setFirstResult(($page-1)*$paginate);
        }
        return array_merge([
            'result' => $queryBuilder->getQuery()->getResult()
        ], ($paginate !== null ? [
            'total' => $total,
            'nb_by_page' => $paginate
        ] : []));
    }

    private function queryExact($field, $fieldValue) {
        return "$field = :$fieldValue";
    }

    private function queryPartial($field, $fieldValue) {
        return "$field like :$fieldValue";
    }

    private function queryIpartial($field, $fieldValue) {
        return "lower($field) like lower(:$fieldValue)";
    }

    private function queryGte($field, $fieldValue) {
        return "$field >= :$fieldValue";
    }

    private function queryEnum($field, $fieldValue, $keys) {
        return "$field in (:$fieldValue".implode(", :$fieldValue", $keys).")";
    }
}
