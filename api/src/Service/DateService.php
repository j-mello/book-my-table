<?php


namespace App\Service;


class DateService
{
    function getWeekDayFromDate(\DateTimeInterface $date) {
        return ($date->format('w')-1+7)%7;
    }

    function findRandomDateForSpecifiedWeekDays(array $days, $inMaxNbDay = 30) {
        $secondsPerDay = 60*60*24;
        $nowTimestamp = floor(date_create()->getTimestamp()/$secondsPerDay)*$secondsPerDay;
        $date = new \DateTime();

        $date->setTimestamp(floor(rand($nowTimestamp,$nowTimestamp+$inMaxNbDay*$secondsPerDay)/$secondsPerDay)*$secondsPerDay);
        while (!in_array($this->getWeekDayFromDate($date),$days)) {
            $date->setTimestamp(floor(rand($nowTimestamp,$nowTimestamp+$inMaxNbDay*$secondsPerDay)/$secondsPerDay)*$secondsPerDay);
        }
        return $date;
    }
}
