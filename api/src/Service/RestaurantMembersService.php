<?php

namespace App\Service;

use App\Entity\Restaurant;
use App\Entity\User;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use App\Entity\RestaurantJoinRequest;

class RestaurantMembersService {
    private $mailerService;
    private $notificationService;
    private $httpClient;

    public function __construct(string $mailerService, string $notificationService, HttpClientInterface $httpClient) {
        $this->mailerService = $mailerService;
        $this->notificationService = $notificationService;
        $this->httpClient = $httpClient;
    }

    public function sendMailRestaurantMemberBanned(Restaurant $restaurant, User $restorer, string $reason) {
        $this->httpClient->request(
            'POST',
            'http://nginx/'.$this->mailerService,
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode([
                    'recipients' => $restorer->getEmail(),
                    'type' => 'bannedFromRestaurant',
                    'firstname' => $restorer->getFirstname(),
                    'lastname' => $restorer->getLastname(),
                    'restaurantName' => $restaurant->getName(),
                    'reason' => $reason
                ])
            ]
        );
    }

    public function sendNotificationRestaurantMemberBanned(Restaurant $restaurant, User $restorer) {
        $this->httpClient->request(
            'POST',
            'http://nginx/'.$this->notificationService,
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode([
                    'usernames' => [$restorer->getEmail()],
                    'type' => 'bannedFromRestaurant',
                    'restaurantName' => $restaurant->getName(),
                    'restaurantId' => $restaurant->getId()
                ])
            ]
        );
    }

    public function sendLeaveRestaurantMail(Restaurant $restaurant, User $restorer, string $reason) {
        $owner = $restaurant->getOwner();
        $this->httpClient->request(
            'POST',
            'http://nginx/'.$this->mailerService,
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode([
                    'recipients' => $owner->getEmail(),
                    'type' => 'leaveRestaurant',
                    'restorerFirstname' => $restorer->getFirstname(),
                    'restorerLastname' => $restorer->getLastname(),
                    'ownerFirstname' => $owner->getFirstname(),
                    'ownerLastname' => $owner->getLastname(),
                    'restaurantName' => $restaurant->getName(),
                    'reason' => $reason
                ])
            ]
        );
    }

    public function sendLeaveRestaurantNotification(Restaurant $restaurant, User $restorer) {
        $owner = $restaurant->getOwner();
        $this->httpClient->request(
            'POST',
            'http://nginx/'.$this->notificationService,
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode([
                    'usernames' => [$owner->getEmail()],
                    'type' => 'leaveRestaurant',
                    'firstname' => $restorer->getFirstname(),
                    'lastname' => $restorer->getLastname(),
                    'restaurantName' => $restaurant->getName(),
                    'restaurantId' => $restaurant->getId()
                ])
            ]
        );
    }

    private function sendMailJoinRequest(RestaurantJoinRequest $restaurantJoinRequest, string $type) {
        $restorer = $restaurantJoinRequest->getRestorer();
        $restaurant = $restaurantJoinRequest->getRestaurant();
        $owner = $restaurant->getOwner();

        $body = [
            'recipients' => $type === 'requestJoinRestaurantReceived' ? $owner->getEmail() : $restorer->getEmail(),
            'type' => $type,
            'firstname' => $type === 'requestJoinRestaurantReceived' ? $owner->getFirstname() : $restorer->getFirstname(),
            'lastname' => $type === 'requestJoinRestaurantReceived' ? $owner->getLastname() : $restorer->getLastname(),
            'restaurantName' => $restaurant->getName()
        ];
        if ($type == 'requestJoinRestaurantReceived') {
            $body = array_merge($body, [
                'applicantFirstname' => $restorer->getFirstname(),
                'applicantLastname' => $restorer->getLastname(),
                'applicantEmail' => $restorer->getEmail(),
                'reason' => $restaurantJoinRequest->getReason(),
            ]);
        }

        $this->httpClient->request(
            'POST',
            'http://nginx/'.$this->mailerService,
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode($body)
            ]
        );
    }

    private function sendNotificationJoinRequest(RestaurantJoinRequest $restaurantJoinRequest, string $type) {
        $restorer = $restaurantJoinRequest->getRestorer();
        $restaurant = $restaurantJoinRequest->getRestaurant();
        $owner = $restaurant->getOwner();

        $body = [
            'usernames' => [($type === "requestJoinRestaurantReceived" ? $owner->getEmail() : $restorer->getEmail())],
            'type' => $type,
            'restaurantName' => $restaurant->getName(),
            'restaurantId' => $restaurant->getId()
        ];
        if ($type == 'requestJoinRestaurantReceived') {
            $body = array_merge($body, [
                'applicantFirstname' => $restorer->getFirstname(),
                'applicantLastname' => $restorer->getLastname(),
                'applicantEmail' => $restorer->getEmail()
            ]);
        }

        $this->httpClient->request(
            'POST',
            'http://nginx/'.$this->notificationService,
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode($body)
            ]
        );
    }

    public function sendMailJoinRequestSent(RestaurantJoinRequest $restaurantJoinRequest) {
        $this->sendMailJoinRequest($restaurantJoinRequest, 'requestJoinRestaurantSent');
    }

    public function sendMailJoinRequestReceived(RestaurantJoinRequest $restaurantJoinRequest) {
        $this->sendMailJoinRequest($restaurantJoinRequest,'requestJoinRestaurantReceived');
    }

    public function sendMailJoinRequestAccepted(RestaurantJoinRequest $restaurantJoinRequest) {
        $this->sendMailJoinRequest($restaurantJoinRequest,'requestJoinRestaurantAccepted');
    }

    public function sendMailJoinRequestDenied(RestaurantJoinRequest $restaurantJoinRequest) {
        $this->sendMailJoinRequest($restaurantJoinRequest,'requestJoinRestaurantDenied');
    }


    public function sendNotificationJoinRequestSent(RestaurantJoinRequest $restaurantJoinRequest) {
        $this->sendNotificationJoinRequest($restaurantJoinRequest, 'requestJoinRestaurantSent');
    }

    public function sendNotificationJoinRequestReceived(RestaurantJoinRequest $restaurantJoinRequest) {
        $this->sendNotificationJoinRequest($restaurantJoinRequest,'requestJoinRestaurantReceived');
    }

    public function sendNotificationJoinRequestAccepted(RestaurantJoinRequest $restaurantJoinRequest) {
        $this->sendNotificationJoinRequest($restaurantJoinRequest,'requestJoinRestaurantAccepted');
    }

    public function sendNotificationJoinRequestDenied(RestaurantJoinRequest $restaurantJoinRequest) {
        $this->sendNotificationJoinRequest($restaurantJoinRequest,'requestJoinRestaurantDenied');
    }
}