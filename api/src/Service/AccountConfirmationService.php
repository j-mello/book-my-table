<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AccountConfirmationService
{
    private $mailerService;
    private $notificationService;
    private $httpClient;
    private $request;

    public function __construct(string $mailerService, string $notificationService, HttpClientInterface $httpClient, RequestStack $requestStack) {
        $this->mailerService = $mailerService;
        $this->notificationService = $notificationService;
        $this->httpClient = $httpClient;
        $this->request = $requestStack->getCurrentRequest();
    }

    public static function generateRandomString($l = 15)
    {
        $token = '';
        $list = 'azertyuiopqsfdghjklmwxcvbnAZERTYUIOPQSFDGHJKLMWXCVBN1234567890!%$';
        for ($i = 0; $i<$l; $i++)
        {
            $token .= $list[rand(0,strlen($list) -1 )];
        }
        return $token;
    }

    private function sendMail(User $user, string $type, string $reason = null) {
        $body = [
            'recipients' => $user->getEmail(),
            'type' => $type,
            'firstname' => $user->getFirstname(),
            'lastname' => $user->getLastname()
        ];

        if (!is_null($reason)) {
            $body['reason'] = $reason;
        }
        if ($type === 'confirmAccount') {
            $body['registerToken'] = $user->getRegisterToken();
            $body['host'] = $this->request->getHost();
        }
        if ($type === "forgottenPassword") {
            $body['newPassword'] = $user->getPlainPassword();
        }

        $this->httpClient->request(
            'POST',
            'http://nginx/'.$this->mailerService,
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode($body)
            ]
        );
    }

    public function sendForgottenPasswordMail(User $user) {
        $this->sendMail($user, 'forgottenPassword');
    }

    public function sendUnBannedMail(User $user, string|null $reason) {
        $this->sendMail($user, 'accountUnbanned', $reason);
    }

    public function sendBannedMail(User $user, string $reason) {
        $this->sendMail($user, 'accountBanned', $reason);
    }

    public function sendRestorerValidatedMail(User $user) {
        $this->sendMail($user, 'accountRestorerValidated');
    }

    public function sendConfirmAccountMail(User $user) {
        $this->sendMail($user, 'confirmAccount');
    }

    public function sendForgottenPasswordNotification(User $user) {
        $this->httpClient->request(
            'POST',
            'http://nginx/'.$this->notificationService,
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode([
                    'type' => 'forgottenPassword',
                    'usernames' => [$user->getEmail()]
                ])
            ]
        );
    }

    public function checkPasswordComplexity($plainPassword) {
        $specialChars = ["@","[","]","^","_","!","\"","#","$","%","&","'","(",")","*","+","-",".","/",":",";","{","}","<",">","=","|","~","?"];

        $maj = false;
        $min = false;
        $number = false;
        $special = false;
        for ($i=0;$i<strlen($plainPassword) && (!$maj || !$min || !$number || !$special);$i++) {
            if (in_array($plainPassword[$i],$specialChars))
                $special = true;
            else if (is_numeric($plainPassword[$i]))
                $number = true;
            else if (strtolower($plainPassword[$i]) != $plainPassword[$i])
                $maj = true;
            else if (strtoupper($plainPassword[$i]) != $plainPassword[$i])
                $min = true;
        }
        return $special && $number && $maj && $min;
    }
}
