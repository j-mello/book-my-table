<?php


namespace App\Service;


use App\Entity\RestaurantTypeSuggestion;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class RestaurantTypeSuggestionConfirmationService
{
    private $mailerService;
    private $notificationService;
    private $httpClient;

    public function __construct(string $mailerService, string $notificationService, HttpClientInterface $httpClient) {
        $this->mailerService = $mailerService;
        $this->notificationService = $notificationService;
        $this->httpClient = $httpClient;
    }

    private function sendTypeSuggestionMail(RestaurantTypeSuggestion $restaurantTypeSuggestion, string $type) {
        $user = $restaurantTypeSuggestion->getProposer();
        $this->httpClient->request(
            'POST',
            'http://nginx/'.$this->mailerService,
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode([
                    'recipients' => $user->getEmail(),
                    'type' => $type,
                    'firstname' => $user->getFirstname(),
                    'lastname' => $user->getLastname(),
                    'suggestedType' => $restaurantTypeSuggestion->getName()
                ])
            ]
        );
    }
    // Mail to send when suggestion created
    public function sendTypeSuggestedMail(RestaurantTypeSuggestion $restaurantTypeSuggestion) {
        $this->sendTypeSuggestionMail(
            $restaurantTypeSuggestion,
            'restaurantTypeSuggested');
    }

    // Mail to send when suggestion accepted
    public function sendSuggestionAcceptedMail(RestaurantTypeSuggestion $restaurantTypeSuggestion) {
        $this->sendTypeSuggestionMail(
            $restaurantTypeSuggestion,
            'restaurantTypeSuggestionAccepted');
    }

    // Mail to send when suggestion denied
    public function sendSuggestionDeniedMail(RestaurantTypeSuggestion $restaurantTypeSuggestion) {
        $this->sendTypeSuggestionMail(
            $restaurantTypeSuggestion,
            'restaurantTypeSuggestionDenied');
    }


    private function sendTypeSuggestionNotification(RestaurantTypeSuggestion $restaurantTypeSuggestion, string $type) {
        $this->httpClient->request(
            'POST',
            'http://nginx/'.$this->notificationService,
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode([
                    'usernames' => [$restaurantTypeSuggestion->getProposer()->getEmail()],
                    'type' => $type,
                    'suggestedType' => $restaurantTypeSuggestion->getName()
                ])
            ]
        );
    }

    // Notification to send when suggestion created
    public function sendTypeSuggestedNotification(RestaurantTypeSuggestion $restaurantTypeSuggestion) {
        $this->sendTypeSuggestionNotification(
            $restaurantTypeSuggestion,
            'restaurantTypeSuggested');
    }

    // Notification to send when suggestion accepted
    public function sendSuggestionAcceptedNotification(RestaurantTypeSuggestion $restaurantTypeSuggestion) {
        $this->sendTypeSuggestionNotification(
            $restaurantTypeSuggestion,
            'restaurantTypeSuggestionAccepted');
    }

    // Notification to send when suggestion denied
    public function sendSuggestionDeniedNotification(RestaurantTypeSuggestion $restaurantTypeSuggestion) {
        $this->sendTypeSuggestionNotification(
            $restaurantTypeSuggestion,
            'restaurantTypeSuggestionDenied');
    }
}
