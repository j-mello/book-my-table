<?php


namespace App\Service;


use App\Entity\Restaurant;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class RestaurantMenuUpdaterService
{
	private $menuService;
	private $httpClient;

	public function __construct(string $menuService, HttpClientInterface $httpClient) {
		$this->menuService = $menuService;
		$this->httpClient = $httpClient;
	}

	public function registerRestaurantInMenuAPI(Restaurant $restaurant) {
		$this->httpClient->request(
			'POST',
			"http://nginx/".$this->menuService."/restaurants",
			[
				'headers' => [
					'Accept' => 'application/json',
					'Content-Type' => 'application/json'
				],
				'body' => json_encode([
					'id' => $restaurant->getId(),
					'ownerMail' => $restaurant->getOwner()->getEmail(),
					'name' => $restaurant->getName()
				])
			]
		);
	}

	public function updateRestaurantInMenuAPI(Restaurant $restaurant) {
		$this->httpClient->request(
			'PUT',
			"http://nginx/".$this->menuService."/restaurants/".$restaurant->getId(),
			[
				'headers' => [
					'Accept' => 'application/json',
					'Content-Type' => 'application/json'
				],
				'body' => json_encode([
					'name' => $restaurant->getName()
				])
			]
		);
	}

	public function deleteRestaurantInMenuAPI(Restaurant $restaurant) {
		$this->httpClient->request(
			'DELETE',
			"http://nginx/".$this->menuService."/restaurants/".$restaurant->getId()
		);
	}
}
