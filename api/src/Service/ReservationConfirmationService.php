<?php

namespace App\Service;


use App\Entity\Reservation;
use App\Entity\User;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ReservationConfirmationService
{
    private $mailerService;
    private $notificationService;
    private $httpClient;
    private $security;

    public function __construct(string $mailerService, string $notificationService, HttpClientInterface $httpClient, Security $security)
    {
        $this->mailerService = $mailerService;
        $this->notificationService = $notificationService;
        $this->httpClient = $httpClient;
        $this->security = $security;
    }

    private function sendReservationMail(Reservation $reservation, string $type, string $reason = null) {
        $requestOptions = [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ],
            'body' => [
                'recipients' => $reservation->getCustomer() ? $reservation->getCustomer()->getEmail() : $reservation->getEmailToConfirm(),
                'type' => $type,
                'restaurantName' => $reservation->getRestaurant()->getName(),
                'serviceName' => $reservation->getSchedule()->getName(),
                'date' => $reservation->getDate()->format("d-m-Y"),
                'nbPersonnes' => $reservation->getNbpersonnes()

            ]
        ];
        if ($reservation->getCustomer() !== null) {
            $requestOptions['body']['firstname'] = $reservation->getCustomer()->getFirstname();
            $requestOptions['body']['lastname'] = $reservation->getCustomer()->getLastname();
        }
        if (!is_null($reason)) {
            $requestOptions['body']['reason'] = $reason;
        }
        $requestOptions['body'] = json_encode($requestOptions['body']);
        $this->httpClient->request(
            'POST',
            'http://nginx/'.$this->mailerService,
            $requestOptions
        );
    }

    private function sendReservationNotification(Reservation $reservation, string $type) {
        $body = [
            'type' => $type,
            'restaurantName' => $reservation->getRestaurant()->getName(),
            'serviceName' => $reservation->getSchedule()->getName(),
            'date' => $reservation->getDate()->format("d-m-Y"),
            'restaurantId' => $reservation->getRestaurant()->getId()
        ];
        if (in_array($type, ['reservationDeletedByRestorer','confirmedReservation'])) {
            $body['usernames'] = [$reservation->getCustomer()->getEmail()];
            $body['nbPersonnes'] = $reservation->getNbpersonnes();
        }
        if ($type !== "reservationDeletedByRestorer") {
            $body['reservationId'] = $reservation->getId();
        }
        if (in_array($type, ['reservationReceivedWithCustomer','reservationReceivedWithoutCustomer'])) {
            $body['usernames'] = [];
            foreach ($reservation->getRestaurant()->getUsers() as $user) {
                $body['usernames'][] = $user->getEmail();
            }

            if ($type === "reservationReceivedWithCustomer") {
                $body['firstname'] = $reservation->getCustomer()->getFirstname();
                $body['lastname'] = $reservation->getCustomer()->getLastname();
            }
        }
        $this->httpClient->request(
            'POST',
            'http://nginx/'.$this->notificationService,
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode($body)
            ]
        );
    }

    public function sendReservationDeletedByRestorerMail(Reservation $reservation, string $reason)
    {
        $this->sendReservationMail($reservation, "reservationDeletedByRestorer", $reason);
    }

    public function sendReservationConfirmationMail(Reservation $reservation)
    {
        $this->sendReservationMail($reservation, "confirmedReservation");
    }


    public function sendReservationDeletedByRestorerNotification(Reservation $reservation)
    {
        $this->sendReservationNotification($reservation, "reservationDeletedByRestorer");
    }

    public function sendReservationConfirmationNotification(Reservation $reservation)
    {
        $this->sendReservationNotification($reservation, "confirmedReservation");
    }

    public function sendReservationReceivedNotification(Reservation $reservation) {
        $this->sendReservationNotification($reservation, $reservation->getCustomer() === null ? 'reservationReceivedWithoutCustomer' : 'reservationReceivedWithCustomer');
    }
}