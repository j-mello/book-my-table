<?php


namespace App\Service;


use App\Entity\Restaurant;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class RestaurantNoteService {
	private $noteService;
	private $httpClient;

	public function __construct(string $noteService, HttpClientInterface $httpClient) {
		$this->noteService = $noteService;
		$this->httpClient = $httpClient;
	}

	public function registerRestaurantInNoteApi(Restaurant $restaurant) {
		$this->httpClient->request(
			'POST',
			'http://nginx/'.$this->noteService.'/'.$restaurant->getId()
		);
	}

	public function deleteRestaurantInNoteApi(Restaurant $restaurant) {
		$this->httpClient->request(
			'DELETE',
			'http://nginx/'.$this->noteService.'/'.$restaurant->getId()
		);
	}
}
