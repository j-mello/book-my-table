<?php


namespace App\Service;


use App\Entity\Restaurant;
use App\Entity\User;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class RestaurantConfirmationService
{
    private $mailerService;
    private $notificationService;
    private $httpClient;
    private $security;

    public function __construct(string $mailerService, string $notificationService, HttpClientInterface $httpClient, Security $security)
    {
        $this->mailerService = $mailerService;
        $this->notificationService = $notificationService;
        $this->httpClient = $httpClient;
        $this->security = $security;
    }

    public function sendRestaurantDeletedByAdmin(Restaurant $restaurant, string $reason){
        $this->sendRestaurantMail($restaurant, $restaurant->getOwner(), "restaurantDeletedByAdmin", $reason);
    }

    public function sendRestaurantCreatedMail(Restaurant $restaurant) {
        $user = $this->security->getUser();
        $this->sendRestaurantMail($restaurant, $user, "restaurantCreated");
    }

    public function sendRestaurantValidatedMail(Restaurant $restaurant) {
        $this->sendRestaurantMail($restaurant, $restaurant->getOwner(), "restaurantValidated");
    }

    public function sendRestaurantDeniedMail(Restaurant $restaurant) {
        $this->sendRestaurantMail($restaurant, $restaurant->getOwner(), "restaurantDenied");
    }

    private function sendRestaurantMail(Restaurant $restaurant, User $user, string $type, string $reason = null) {
        $requestOptions = [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ],
            'body' => [
                'recipients' => $user->getEmail(),
                'type' => $type,
                'firstname' => $user->getFirstname(),
                'lastname' => $user->getLastname(),
                'name' => $restaurant->getName()
            ]
        ];
        if (!is_null($reason)) {
            $requestOptions['body']['reason'] = $reason;
        }
        $requestOptions['body'] = json_encode($requestOptions['body']);
        $this->httpClient->request(
            'POST',
            'http://nginx/'.$this->mailerService,
            $requestOptions
        );
    }



    public function sendRestaurantCreatedNotification(Restaurant $restaurant) {
        $user = $this->security->getUser();
        $this->sendRestaurantNotification($restaurant, $user, "restaurantCreated");
    }

    public function sendRestaurantDeletedByAdminNotification(Restaurant $restaurant) {
        $this->sendRestaurantNotification($restaurant, $restaurant->getOwner(), "restaurantDeletedByAdmin");
    }

    public function sendRestaurantValidatedNotification(Restaurant $restaurant) {
        $this->sendRestaurantNotification($restaurant, $restaurant->getOwner(), "restaurantValidated");
    }

    public function sendRestaurantDeniedNotification(Restaurant $restaurant) {
        $this->sendRestaurantNotification($restaurant, $restaurant->getOwner(), "restaurantDenied");
    }

    private function sendRestaurantNotification(Restaurant $restaurant, User $user, string $type) {
        $this->httpClient->request(
            'POST',
            'http://nginx/'.$this->notificationService,
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode([
                    'usernames' => [$user->getEmail()],
                    'type' => $type,
                    'name' => $restaurant->getName(),
                    'restaurantId' => $restaurant->getId()
                ])
            ]
        );
    }


}
