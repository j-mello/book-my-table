<?php

namespace App\Controller\User;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Service\AccountConfirmationService;

class ForgottenPasswordController extends AbstractController
{
    public function __invoke(User $data, UserRepository $userRepository, AccountConfirmationService $accountConfirmationService): User
    {
        if ($data->getEmail() === null) {
            throw new BadRequestException("Vous n'avez pas mentionné d'adresse mail");
        }
        $user = $userRepository->findOneByEmail($data->getEmail());

        if (!($user instanceof User)) {
            throw new NotFoundHttpException("Cette adresse mail n'existe pas");
        }

        do {
            $newPassword = AccountConfirmationService::generateRandomString(20);
        } while (!$accountConfirmationService->checkPasswordComplexity($newPassword));

        $user->setPlainPassword($newPassword);
        $user->setPassword($newPassword);

        $accountConfirmationService->sendForgottenPasswordMail($user);
        $accountConfirmationService->sendForgottenPasswordNotification($user);

        $this->getDoctrine()->getManager()->flush();

        return $user;
    }
}
