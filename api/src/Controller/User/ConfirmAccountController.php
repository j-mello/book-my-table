<?php

namespace App\Controller\User;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConfirmAccountController extends AbstractController
{
    public function __invoke(Request $request, UserRepository $userRepository): Response
    {
        $user = $userRepository->findByRegisterToken(json_decode($request->getContent())[0]->registerToken);
        if (!empty($user)) {
            $user->setRegisterToken(null);
            $this->getDoctrine()->getManager()->flush();
        }
        return new Response(null, empty($user) ? 404 : 204);
    }
}
