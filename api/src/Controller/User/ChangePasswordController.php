<?php

namespace App\Controller\User;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Response;
use App\Custom\Validator\Constraints as CustomAssert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ChangePasswordController extends AbstractController
{
    public function __invoke(User $user, ValidatorInterface $validator): Response
    {
        $complexityPasswordAssert = new CustomAssert\ComplexityPassword([
        	"message" => "Votre mot de passe n'est pas suffisament complexe"
		]);
        $errors = $validator->validate(
            $user->getPlainPassword(),
            $complexityPasswordAssert
        );
        if ($errors->count()) {
			throw new BadRequestException($complexityPasswordAssert->message);
		}

        $user->setPassword($user->getPlainPassword());

        $this->getDoctrine()->getManager()->flush();

        return new Response(null, 200);
    }
}
