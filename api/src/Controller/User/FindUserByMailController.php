<?php

namespace App\Controller\User;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FindUserByMailController extends AbstractController
{
    public function __invoke(Request $request, UserRepository $userRepository): User
    {
    	$email = $request->get("email");
    	if ($email === null || $email === "") {
    		throw new BadRequestException();
		}
    	$user = $userRepository->findNonAdminUserByEmail($email);
    	if ($user === null) {
			throw new NotFoundHttpException();
		}
    	return $user;
    }
}
