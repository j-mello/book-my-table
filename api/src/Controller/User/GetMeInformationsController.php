<?php

namespace App\Controller\User;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GetMeInformationsController extends AbstractController
{
    public function __invoke(): User
    {
        return $this->getUser();
    }
}
