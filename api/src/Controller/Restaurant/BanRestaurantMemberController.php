<?php

namespace App\Controller\Restaurant;


use App\Entity\Restaurant;
use App\Service\RestaurantMembersService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BanRestaurantMemberController extends AbstractController
{
    public function __invoke(Restaurant $restaurant, Request $request, RestaurantMembersService $restaurantMembersService): Response
    {
        $content = json_decode($request->getContent());
        if ($content == null || !property_exists($content, 'reason') || !property_exists($content, 'restorerId')) {
            throw new BadRequestException("Vous devez mentionner les paramètres 'reason' et 'restorerId'");
        }
        $restorerId = $content->restorerId;
        if (!is_numeric($restorerId)) {
            throw new BadRequestException("'restorerId' incorrect");
        }
        $restorerId = (int)$restorerId;
        if ($restorerId === $this->getUser()->getId()) {
            throw new BadRequestException("Vous ne pouvez pas vous auto ban");
        }

        $reason = $content->reason;

        if (trim($reason) == "") {
            throw new BadRequestException("Vous devez mentionner une raison");
        }

        $restorerToBan = null;
        foreach ($restaurant->getUsers() as $restorer) {
            if ($restorer->getId() === $restorerId) {
                $restorerToBan = $restorer;
                break;
            }
        }
        if ($restorerToBan === null) {
            throw new NotFoundHttpException("Restaurateur '$restorerId' introuvable dans ce restaurant");
        }

        $restaurantMembersService->sendMailRestaurantMemberBanned($restaurant, $restorerToBan, trim($reason));
        $restaurantMembersService->sendNotificationRestaurantMemberBanned($restaurant, $restorerToBan);

        $restaurant->removeUser($restorerToBan);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return new Response(null, 204);
    }
}
