<?php

namespace App\Controller\Restaurant;

use App\Entity\Restaurant;
use App\Service\RestaurantConfirmationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Response;

class DenyRestaurantController extends AbstractController
{
    public function __invoke(Restaurant $restaurant, RestaurantConfirmationService $restaurantConfirmationService): Response
    {
        if ($restaurant->getValidated()) {
            throw new BadRequestException("Vous ne pouvez pas refuser un restaurant qui a déjà été validé");
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($restaurant);
        $em->flush();

        $restaurantConfirmationService->sendRestaurantDeniedMail($restaurant);
        $restaurantConfirmationService->sendRestaurantDeniedNotification($restaurant);

        return new Response(null, 201);
    }
}
