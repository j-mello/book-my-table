<?php

namespace App\Controller\Restaurant;


use App\Entity\Restaurant;
use App\Service\RestaurantMembersService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LeaveRestaurantController extends AbstractController
{
    public function __invoke(Restaurant $restaurant, Request $request, RestaurantMembersService $restaurantMembersService): Response
    {
        $content = json_decode($request->getContent());
        if ($content === null || !property_exists($content, 'reason') || trim($content->reason) === "") {
            throw new BadRequestException("Vous devez mentionner une raison");
        }

        $reason = $content->reason;

        $restaurantMembersService->sendLeaveRestaurantMail($restaurant,$this->getUser(),trim($reason));
        $restaurantMembersService->sendLeaveRestaurantNotification($restaurant,$this->getUser());

        $restaurant->removeUser($this->getUser());

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return new Response(null, 204);
    }
}
