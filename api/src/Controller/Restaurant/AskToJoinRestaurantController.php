<?php

namespace App\Controller\Restaurant;

use App\Entity\Restaurant;
use App\Entity\RestaurantJoinRequest;
use App\Repository\RestaurantJoinRequestRepository;
use App\Service\RestaurantMembersService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AskToJoinRestaurantController extends AbstractController
{
    public function __invoke(Restaurant $restaurant, RestaurantJoinRequestRepository $restaurantJoinRequestRepository, Request $request, RestaurantMembersService $restaurantMembersService): Response
    {
        $existingJoinRequest = $restaurantJoinRequestRepository->findOneByRestaurantIdAndRestorerId($restaurant->getId(),$this->getUser()->getId());
        if ($existingJoinRequest !== null) {
            throw new Exception("Vous avez déjà demandé à rejoindre ce restaurant");
        }

        $content = json_decode($request->getContent());
        if (!property_exists($content,'reason')) {
            throw new BadRequestException("Vous devez spécifier la raison pour laquelle vous souhaitez rejoindre ce restaurant");
        }
        $reason = $content->reason;

        $restaurantJoinRequest = new RestaurantJoinRequest();
        $restaurantJoinRequest->setRestaurant($restaurant);
        $restaurantJoinRequest->setRestorer($this->getUser());
        $restaurantJoinRequest->setReason($reason);

        $restaurantMembersService->sendMailJoinRequestSent($restaurantJoinRequest);
        $restaurantMembersService->sendMailJoinRequestReceived($restaurantJoinRequest);

        $restaurantMembersService->sendNotificationJoinRequestSent($restaurantJoinRequest);
        $restaurantMembersService->sendNotificationJoinRequestReceived($restaurantJoinRequest);

        $em = $this->getDoctrine()->getManager();
        $em->persist($restaurantJoinRequest);
        $em->flush();

        return new Response(null, 201);
    }
}
