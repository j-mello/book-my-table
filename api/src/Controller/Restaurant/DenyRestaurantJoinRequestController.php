<?php

namespace App\Controller\Restaurant;


use App\Entity\RestaurantJoinRequest;
use App\Service\RestaurantMembersService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DenyRestaurantJoinRequestController extends AbstractController
{
    public function __invoke(RestaurantJoinRequest $restaurantJoinRequest, RestaurantMembersService $restaurantMembersService): Response
    {
        $restaurantMembersService->sendMailJoinRequestDenied($restaurantJoinRequest);
        $restaurantMembersService->sendNotificationJoinRequestDenied($restaurantJoinRequest);

        $em = $this->getDoctrine()->getManager();
        $em->remove($restaurantJoinRequest);;
        $em->flush();

        return new Response(null, 201);
    }
}
