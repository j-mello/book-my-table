<?php

namespace App\Controller\Restaurant;

use App\Entity\Restaurant;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

class CheckIfIsMemberInRestaurantController extends AbstractController
{
    public function __invoke(Restaurant $restaurant, Security $security): Response {
        return new Response(null, $restaurant->hasUser($security->getUser()) ? 204 : 404);
    }
}
