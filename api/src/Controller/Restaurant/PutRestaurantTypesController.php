<?php

namespace App\Controller\Restaurant;

use App\Entity\Restaurant;
use App\Repository\RestaurantTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;

class PutRestaurantTypesController extends AbstractController
{
    public function __invoke(Restaurant $restaurant, RestaurantTypeRepository $restaurantTypeRepository, Request $request): Restaurant
    {
        $content = json_decode($request->getContent());
        if (!property_exists($content, 'types')) {
            throw new BadRequestException("Vous devez mentionner les types séparé de virgule");
        }

        $restaurantTypes = array_map(function ($id) use($restaurantTypeRepository) {
            $id = trim($id);
            if (!is_numeric($id))
                throw new BadRequestException("Id de type de restaurant mal renseigné");

            $restaurantType = $restaurantTypeRepository->find((int)$id);
            if ($restaurantType == null)
                throw new BadRequestException("Le restaurant à l'id '$id' n'existe pas");

            return $restaurantType;
        }, explode(",",$content->types));

        foreach ($restaurant->getRestaurantTypes() as $restaurantType) {
            $restaurant->removeRestaurantType($restaurantType);
        }
        foreach ($restaurantTypes as $restaurantType) {
            $restaurant->addRestaurantType($restaurantType);
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $restaurant;
    }
}
