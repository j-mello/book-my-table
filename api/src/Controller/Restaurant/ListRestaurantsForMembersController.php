<?php

namespace App\Controller\Restaurant;

use App\Entity\Restaurant;
use App\Repository\RestaurantRepository;
use App\Service\AdvancedSearchFilterService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ListRestaurantsForMembersController extends AbstractController
{
    public function __invoke(Request $request, RestaurantRepository $restaurantRepository, AdvancedSearchFilterService $advancedSearchFilterService): iterable
    {
        $filters = [
            "name" => [
                "type" => "ipartial"
            ],
            "nbPlace" => [
                "type" => "gte"
            ],
            "city" => [
                "type" => "ipartial"
            ],
            "zipcode" => [
                "type" => "ipartial"
            ],
            "address" => [
                "type" => "ipartial"
            ],
            "phonenumber" => [
                "type" => "ipartial"
            ],
            "validated" => [
                "type" => "exact"
            ],
            "restaurantTypes" => [
                "type" => "enum",
                "modelColumn" => "id"
            ],
            ($request->get('_route') === 'api_users_restaurants_get_subresource' ? "users" : "owner") => [
                "type" => "exact",
                "modelColumn" => "id",
                "default" => (int)$request->attributes->get("id"),
                "modifiable" => false
            ]
        ];

        $datas = $advancedSearchFilterService->checkFilters($filters, Restaurant::class);
        $paginate = in_array($request->get('paginate'),[null,'true']);
        return $restaurantRepository->findByFilters($datas, $paginate ? 5 : null);
    }
}
