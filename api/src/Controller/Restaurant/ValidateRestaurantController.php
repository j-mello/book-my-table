<?php

namespace App\Controller\Restaurant;

use App\Entity\Restaurant;
use App\Service\RestaurantConfirmationService;
use App\Service\RestaurantMenuUpdaterService;
use App\Service\RestaurantNoteService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ValidateRestaurantController extends AbstractController
{
    public function __invoke(
    	Restaurant $restaurant,
		RestaurantConfirmationService $restaurantConfirmationService,
		RestaurantMenuUpdaterService $restaurantMenuUpdaterService,
		RestaurantNoteService $restaurantNoteService): Restaurant
    {
        if (!$restaurant->getValidated()) {
            $restaurant->setValidated(true);
            $this->getDoctrine()->getManager()->flush();
            $restaurantConfirmationService->sendRestaurantValidatedMail($restaurant);
            $restaurantConfirmationService->sendRestaurantValidatedNotification($restaurant);

            $restaurantMenuUpdaterService->registerRestaurantInMenuAPI($restaurant);
			$restaurantNoteService->registerRestaurantInNoteApi($restaurant);
        }

        return $restaurant;
    }
}
