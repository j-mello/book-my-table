<?php

namespace App\Controller\RestaurantTypeSuggestion;

use App\Entity\RestaurantTypeSuggestion;
use App\Repository\RestaurantTypeRepository;
use App\Repository\RestaurantTypeSuggestionRepository;
use App\Service\RestaurantTypeSuggestionConfirmationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Config\Definition\Exception\Exception;

class ProposeNewTypeController extends AbstractController
{
    public function __invoke(RestaurantTypeSuggestion $data, RestaurantTypeSuggestionConfirmationService $restaurantTypeSuggestionConfirmationService, RestaurantTypeRepository $restaurantTypeRepository, RestaurantTypeSuggestionRepository $restaurantTypeSuggestionRepository): RestaurantTypeSuggestion
    {
        if ($restaurantTypeSuggestionRepository->findOneByName($data->getName()) !== null)
            throw new Exception("Il y a déjà une proposition de type avec ce nom");

        if ($restaurantTypeRepository->findOneByName($data->getName()) !== null)
            throw new Exception("Il y a déjà un type portant ce nom");


        $data->setProposer($this->getUser());

        $restaurantTypeSuggestionConfirmationService->sendTypeSuggestedMail($data);
        $restaurantTypeSuggestionConfirmationService->sendTypeSuggestedNotification($data);

        return $data;
    }
}
