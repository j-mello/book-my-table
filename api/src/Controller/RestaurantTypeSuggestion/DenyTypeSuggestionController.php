<?php

namespace App\Controller\RestaurantTypeSuggestion;

use App\Entity\RestaurantTypeSuggestion;
use App\Service\RestaurantTypeSuggestionConfirmationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DenyTypeSuggestionController extends AbstractController
{
    public function __invoke(RestaurantTypeSuggestion $restaurantTypeSuggestion, RestaurantTypeSuggestionConfirmationService $restaurantTypeSuggestionConfirmationService): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($restaurantTypeSuggestion);
        $em->flush();

        $restaurantTypeSuggestionConfirmationService->sendSuggestionDeniedMail($restaurantTypeSuggestion);
        $restaurantTypeSuggestionConfirmationService->sendSuggestionDeniedNotification($restaurantTypeSuggestion);

        return new Response(null, 201);
    }
}
