<?php

namespace App\Controller\RestaurantTypeSuggestion;

use App\Entity\RestaurantType;
use App\Entity\RestaurantTypeSuggestion;
use App\Service\RestaurantTypeSuggestionConfirmationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class AcceptTypeSuggestionController extends AbstractController
{
    public function __invoke(RestaurantTypeSuggestion $restaurantTypeSuggestion, RestaurantTypeSuggestionConfirmationService $restaurantTypeSuggestionConfirmationService): Response
    {
        $restaurantType = new RestaurantType();
        $restaurantType->setName($restaurantTypeSuggestion->getName());

        $em = $this->getDoctrine()->getManager();
        $em->persist($restaurantType);
        $em->remove($restaurantTypeSuggestion);
        $em->flush();

        $restaurantTypeSuggestionConfirmationService->sendSuggestionAcceptedMail($restaurantTypeSuggestion);
        $restaurantTypeSuggestionConfirmationService->sendSuggestionAcceptedNotification($restaurantTypeSuggestion);

        return new Response(null, 201);
    }
}
