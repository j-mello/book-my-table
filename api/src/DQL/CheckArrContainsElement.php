<?php
namespace App\DQL;


use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\SqlWalker;
use Doctrine\ORM\Query\Parser;

class CheckArrContainsElement extends FunctionNode
{
    private $arr = null;
    private $elem = null;

    public function getSql(SqlWalker $sqlWalker)
    {
        return 'check_arr_contains_element('.$this->arr->dispatch($sqlWalker).','.$this->elem->dispatch($sqlWalker).')';
    }

    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->arr = $parser->StringPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->elem = $parser->StringPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}
