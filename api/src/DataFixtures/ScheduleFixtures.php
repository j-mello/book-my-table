<?php

namespace App\DataFixtures;

use App\Entity\Schedule;
use App\Repository\RestaurantRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ScheduleFixtures extends Fixture implements DependentFixtureInterface
{
    private $restaurantRepository;

    public function __construct(RestaurantRepository $restaurantRepository) {
        $this->restaurantRepository = $restaurantRepository;
    }

    public function load(ObjectManager $manager): void
    {
        $restaurants = $this->restaurantRepository->findAll();

        foreach ($restaurants as $restaurant) {
            $morning = new Schedule();
            $morning
                ->setNbPlace(rand(max($restaurant->getNbPlace()-10,3),$restaurant->getNbPlace()))
                ->setName('Matin')
                ->setStart(\DateTime::createFromFormat('H:i', '07:00'))
                ->setEnding(\DateTime::createFromFormat('H:i', '10:00'))
                ->setRestaurant($restaurant);
            $manager->persist($morning);

            $midday = new Schedule();
            $midday
                ->setNbPlace(rand($restaurant->getNbPlace()-10,$restaurant->getNbPlace()))
                ->setName('Midi')
                ->setStart(\DateTime::createFromFormat('H:i', '11:00'))
                ->setEnding(\DateTime::createFromFormat('H:i', '14:00'))
                ->setRestaurant($restaurant);
            $manager->persist($midday);

            $evening = new Schedule();
            $evening
                ->setNbPlace(rand($restaurant->getNbPlace()-10,$restaurant->getNbPlace()))
                ->setName('Soir')
                ->setStart(\DateTime::createFromFormat('H:i', '19:00'))
                ->setEnding(\DateTime::createFromFormat('H:i', '23:00'))
                ->setRestaurant($restaurant);
            $manager->persist($evening);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            RestaurantFixtures::class
        ];
    }
}
