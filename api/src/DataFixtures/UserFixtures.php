<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class UserFixtures extends Fixture
{

    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');

        $admin = new User();
        $admin
            ->setEmail("admin@admin.com")
            ->setFirstname("admin")
            ->setLastname("admin")
            ->setRoles([User::ROLE_ADMIN])
            ->setPlainPassword("1234")
            ->setRegisterToken(null)
        ;
        $manager->persist($admin);

        $restorer = new User();
        $restorer
            ->setEmail("restorer@restorer.com")
            ->setFirstname("restorer")
            ->setLastname("restorer")
            ->setRoles([User::ROLE_RESTORER])
            ->setPlainPassword("1234")
            ->setRegisterToken(null)
            ->setRestorerValidated(true)
        ;
        $manager->persist($restorer);

        $user = new User();
        $user
            ->setEmail("user@user.com")
            ->setFirstname("user")
            ->setLastname("user")
            ->setPlainPassword("1234")
            ->setRegisterToken(null)
        ;
        $manager->persist($user);



        for ($i=0;$i<50;$i++) {
            $role = rand(0,1) == 0 ? User::ROLE_USER : User::ROLE_RESTORER;

            $user = new User();
            $user
                ->setEmail($faker->email)
                ->setFirstname($faker->firstName)
                ->setLastname($faker->lastName)
                ->setRoles([$role])
                ->setPlainPassword('1234')
                ->setRegisterToken(null)
            ;
            if ($role == User::ROLE_RESTORER) {
                $user->setRestorerValidated(true);

            }
            $manager->persist($user);
        }

        $manager->flush();
    }
}
