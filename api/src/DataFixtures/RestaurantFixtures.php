<?php

namespace App\DataFixtures;

use App\Entity\Restaurant;
use App\Entity\User;
use App\Repository\RestaurantTypeRepository;
use App\Repository\UserRepository;
use App\Service\RestaurantMenuUpdaterService;
use App\Service\RestaurantNoteService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker;

class RestaurantFixtures extends Fixture implements DependentFixtureInterface
{
    private $restaurantTypeRepository;
    private $userRepository;
    private $restaurantMenuUpdaterService;
    private $restaurantNoteService;

    public function __construct(RestaurantTypeRepository $restaurantTypeRepository, UserRepository $userRepository, RestaurantMenuUpdaterService $restaurantMenuUpdaterService, RestaurantNoteService $restaurantNoteService) {
        $this->restaurantTypeRepository = $restaurantTypeRepository;
        $this->userRepository = $userRepository;
        $this->restaurantMenuUpdaterService = $restaurantMenuUpdaterService;
        $this->restaurantNoteService = $restaurantNoteService;
    }

    public function load(ObjectManager $manager): void
    {
        $restaurantTypes = $this->restaurantTypeRepository->findAll();
        $restorers = $this->userRepository->findByRole(User::ROLE_RESTORER);

        $faker = Faker\Factory::create('fr_FR');

        for ($i=0;$i<10;$i++) {
            $restaurant = new Restaurant();
            $restaurant
                ->setName($faker->company)
                ->setAddress($faker->address)
                ->setCity($faker->city)
                ->setZipcode($faker->postcode)
                ->setNbPlace(rand(10,50))
                ->setPhonenumber($faker->phoneNumber)
                ->setValidated(true);
            ;
            $nbTypes = rand(1,2);
            for ($j=0;$j<$nbTypes;$j++) {
                $restaurant->addRestaurantType($restaurantTypes[array_rand($restaurantTypes)]);
            }
            $manager->persist($restaurant);
			$restaurant_restorers = [];
			for ($j=0;$j<4;$j++) {
				$restorer_index = array_rand($restorers);
				while ($restaurant->hasUser($restorers[$restorer_index]))
					$restorer_index = array_rand($restorers);
				$restaurant->addUser($restorers[$restorer_index]);
				$restaurant_restorers[] = $restorers[$restorer_index];
			}
			$restaurant->setOwner($restaurant_restorers[array_rand($restaurant_restorers)]);
            $this->restaurantMenuUpdaterService->registerRestaurantInMenuAPI($restaurant);
            $this->restaurantNoteService->registerRestaurantInNoteApi($restaurant);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
            RestaurantTypeFixtures::class
        ];
    }
}
