<?php

namespace App\DataFixtures;

use App\Entity\Reservation;
use App\Repository\ScheduleRepository;
use App\Repository\UserRepository;
use App\Service\DateService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ReservationFixtures extends Fixture implements DependentFixtureInterface
{
    private $scheduleRepository;
    private $userRepository;
    private $dateService;

    public function __construct(ScheduleRepository $scheduleRepository, UserRepository $userRepository, DateService $dateService) {
        $this->scheduleRepository = $scheduleRepository;
        $this->userRepository = $userRepository;
        $this->dateService = $dateService;
    }

    public function load(ObjectManager $manager): void
    {
        $schedules = $this->scheduleRepository->findAll();
        $users = $this->userRepository->findAll();

        for ($i=0;$i<40;$i++) {
            $schedule = $schedules[array_rand($schedules)];

            $reservation = new Reservation();
            $reservation
                ->setCustomer($users[array_rand($users)])
                ->setDate($this->dateService->findRandomDateForSpecifiedWeekDays($schedule->getDays()))
                ->setNbpersonnes(rand(2,7))
                ->setSchedule($schedule)
                ->setRestaurant($schedule->getRestaurant())
            ;
            $manager->persist($reservation);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ScheduleFixtures::class
        ];
    }
}
