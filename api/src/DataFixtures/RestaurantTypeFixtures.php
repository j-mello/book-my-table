<?php

namespace App\DataFixtures;

use App\Entity\RestaurantType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RestaurantTypeFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        foreach (['A volonté','Fastfood','Asiatique','Français','Grillade','Snack','Libanais','Japonais','Chinois','Thailandais','Marocain','Tunisien','Kebab'] as $type) {
            $restaurantType = new RestaurantType();
            $restaurantType->setName($type);
            $manager->persist($restaurantType);
        }

        $manager->flush();
    }
}
