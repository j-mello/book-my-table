<?php

namespace App\Repository;

use App\Entity\RestaurantTypeSuggestion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RestaurantTypeSuggestion|null find($id, $lockMode = null, $lockVersion = null)
 * @method RestaurantTypeSuggestion|null findOneBy(array $criteria, array $orderBy = null)
 * @method RestaurantTypeSuggestion[]    findAll()
 * @method RestaurantTypeSuggestion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestaurantTypeSuggestionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RestaurantTypeSuggestion::class);
    }

    // /**
    //  * @return RestaurantTypeSuggestion[] Returns an array of RestaurantTypeSuggestion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RestaurantTypeSuggestion
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findOneByName($name): ?RestaurantTypeSuggestion
    {
        return $this->createQueryBuilder('r')
            ->andWhere('LOWER(r.name) = LOWER(:name)')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
