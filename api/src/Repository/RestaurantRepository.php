<?php

namespace App\Repository;

use App\Entity\Restaurant;
use App\Service\AdvancedSearchFilterService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Restaurant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Restaurant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Restaurant[]    findAll()
 * @method Restaurant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestaurantRepository extends ServiceEntityRepository
{
    private $advancedSearchFilterService;

    public function __construct(ManagerRegistry $registry, AdvancedSearchFilterService $advancedSearchFilterService)
    {
        parent::__construct($registry, Restaurant::class);
        $this->advancedSearchFilterService = $advancedSearchFilterService;
    }

    public function findByFilters($datas, $paginate = null) {
        return $this->advancedSearchFilterService->applyFilters(
            $this->createQueryBuilder('r'),
            $datas,
            $paginate
        );
    }

    // /**
    //  * @return Restaurant[] Returns an array of Restaurant objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Restaurant
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
