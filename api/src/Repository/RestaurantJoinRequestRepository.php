<?php

namespace App\Repository;

use App\Entity\RestaurantJoinRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RestaurantJoinRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method RestaurantJoinRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method RestaurantJoinRequest[]    findAll()
 * @method RestaurantJoinRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestaurantJoinRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RestaurantJoinRequest::class);
    }

    public function findOneByRestaurantIdAndRestorerId($restaurantId, $restorerId) {
        return $this->createQueryBuilder('r')
            ->where("r.restorer = :restorer")
            ->andWhere("r.restaurant = :restaurant")
            ->setParameter("restorer", $restorerId)
            ->setParameter("restaurant", $restaurantId)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    // /**
    //  * @return RestaurantJoinRequest[] Returns an array of RestaurantJoinRequest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RestaurantJoinRequest
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
