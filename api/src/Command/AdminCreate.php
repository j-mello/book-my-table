<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AdminCreate extends Command
{

    protected static $defaultName = 'admin:create';
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {
        $this
            ->setDescription("Créer un compte administrateur")
            ->addArgument("email", InputArgument::REQUIRED)
            ->addArgument("firstname", InputArgument::REQUIRED)
            ->addArgument("lastname", InputArgument::REQUIRED)
            ->addArgument("password", InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $user = new User();
        $user
            ->setEmail($input->getArgument("email"))
            ->setFirstname($input->getArgument("firstname"))
            ->setLastname($input->getArgument("lastname"))
            ->setPlainPassword($input->getArgument("password"))
            ->setRoles(['ROLE_ADMIN'])
            ->setRegisterToken(null);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $output->writeln('Admin created!');

        return Command::SUCCESS;
    }
}
