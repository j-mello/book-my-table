<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AdminList extends Command
{

    protected static $defaultName = 'admin:list';
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
    }

    protected function configure(): void
    {
        $this
            ->setDescription("Lister les comptes administrateurs");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $admins = $this->userRepository->findByRole(User::ROLE_ADMIN);
        $output->writeln('id | email | firstname | lastname');
        foreach ($admins as $admin) {
            $output->writeln($admin->getId()." | ".$admin->getEmail()." | ".$admin->getFirstname()." | ".$admin->getLastname());
        }

        return Command::SUCCESS;
    }
}
