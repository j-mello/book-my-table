<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211206104921 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("create or replace function check_arr_contains_element(arr json, elem varchar)
                                returns bool
                                language plpgsql
                            as
                            $$
                            begin
                                return (arr)::jsonb ?? elem;
                            end;
                            $$;");
        $this->addSql('CREATE SEQUENCE reservation_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE restaurant_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE restaurant_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE restaurant_type_suggestion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE schedule_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE reservation (id INT NOT NULL, customer_id INT NOT NULL, schedule_id INT NOT NULL, restaurant_id INT NOT NULL, date DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_42C849559395C3F3 ON reservation (customer_id)');
        $this->addSql('CREATE INDEX IDX_42C84955A40BC2D5 ON reservation (schedule_id)');
        $this->addSql('CREATE INDEX IDX_42C84955B1E7706E ON reservation (restaurant_id)');
        $this->addSql('CREATE TABLE restaurant (id INT NOT NULL, owner_id INT NOT NULL, name VARCHAR(255) NOT NULL, nb_place INT NOT NULL, city VARCHAR(255) NOT NULL, zipcode VARCHAR(6) NOT NULL, address VARCHAR(255) NOT NULL, phonenumber VARCHAR(20) NOT NULL, validated BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_EB95123F7E3C61F9 ON restaurant (owner_id)');
        $this->addSql('CREATE TABLE restaurant_type (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE restaurant_type_restaurant (restaurant_type_id INT NOT NULL, restaurant_id INT NOT NULL, PRIMARY KEY(restaurant_type_id, restaurant_id))');
        $this->addSql('CREATE INDEX IDX_3069D2F8F07D0E6 ON restaurant_type_restaurant (restaurant_type_id)');
        $this->addSql('CREATE INDEX IDX_3069D2F8B1E7706E ON restaurant_type_restaurant (restaurant_id)');
        $this->addSql('CREATE TABLE restaurant_type_suggestion (id INT NOT NULL, proposer_id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_67C33DCB13FA634 ON restaurant_type_suggestion (proposer_id)');
        $this->addSql('CREATE TABLE schedule (id INT NOT NULL, restaurant_id INT NOT NULL, name VARCHAR(255) DEFAULT NULL, start TIME(0) WITHOUT TIME ZONE NOT NULL, ending TIME(0) WITHOUT TIME ZONE NOT NULL, nb_place INT NOT NULL, days JSON NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5A3811FBB1E7706E ON schedule (restaurant_id)');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, email VARCHAR(180) NOT NULL, firstname VARCHAR(20) NOT NULL, lastname VARCHAR(20) NOT NULL, register_token VARCHAR(255) DEFAULT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, restorer_validated BOOLEAN NOT NULL, banned BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('CREATE TABLE user_restaurant (user_id INT NOT NULL, restaurant_id INT NOT NULL, PRIMARY KEY(user_id, restaurant_id))');
        $this->addSql('CREATE INDEX IDX_4CF2D4D3A76ED395 ON user_restaurant (user_id)');
        $this->addSql('CREATE INDEX IDX_4CF2D4D3B1E7706E ON user_restaurant (restaurant_id)');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C849559395C3F3 FOREIGN KEY (customer_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C84955A40BC2D5 FOREIGN KEY (schedule_id) REFERENCES schedule (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C84955B1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE restaurant ADD CONSTRAINT FK_EB95123F7E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE restaurant_type_restaurant ADD CONSTRAINT FK_3069D2F8F07D0E6 FOREIGN KEY (restaurant_type_id) REFERENCES restaurant_type (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE restaurant_type_restaurant ADD CONSTRAINT FK_3069D2F8B1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE restaurant_type_suggestion ADD CONSTRAINT FK_67C33DCB13FA634 FOREIGN KEY (proposer_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE schedule ADD CONSTRAINT FK_5A3811FBB1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_restaurant ADD CONSTRAINT FK_4CF2D4D3A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_restaurant ADD CONSTRAINT FK_4CF2D4D3B1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("drop function check_arr_contains_element(arr json, elem varchar);");
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE reservation DROP CONSTRAINT FK_42C84955B1E7706E');
        $this->addSql('ALTER TABLE restaurant_type_restaurant DROP CONSTRAINT FK_3069D2F8B1E7706E');
        $this->addSql('ALTER TABLE schedule DROP CONSTRAINT FK_5A3811FBB1E7706E');
        $this->addSql('ALTER TABLE user_restaurant DROP CONSTRAINT FK_4CF2D4D3B1E7706E');
        $this->addSql('ALTER TABLE restaurant_type_restaurant DROP CONSTRAINT FK_3069D2F8F07D0E6');
        $this->addSql('ALTER TABLE reservation DROP CONSTRAINT FK_42C84955A40BC2D5');
        $this->addSql('ALTER TABLE reservation DROP CONSTRAINT FK_42C849559395C3F3');
        $this->addSql('ALTER TABLE restaurant DROP CONSTRAINT FK_EB95123F7E3C61F9');
        $this->addSql('ALTER TABLE restaurant_type_suggestion DROP CONSTRAINT FK_67C33DCB13FA634');
        $this->addSql('ALTER TABLE user_restaurant DROP CONSTRAINT FK_4CF2D4D3A76ED395');
        $this->addSql('DROP SEQUENCE reservation_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE restaurant_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE restaurant_type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE restaurant_type_suggestion_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE schedule_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('DROP TABLE reservation');
        $this->addSql('DROP TABLE restaurant');
        $this->addSql('DROP TABLE restaurant_type');
        $this->addSql('DROP TABLE restaurant_type_restaurant');
        $this->addSql('DROP TABLE restaurant_type_suggestion');
        $this->addSql('DROP TABLE schedule');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE user_restaurant');
    }
}
