<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211229135439 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE restaurant_join_request_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE restaurant_join_request (id INT NOT NULL, restaurant_id INT DEFAULT NULL, restorer_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_140166BFB1E7706E ON restaurant_join_request (restaurant_id)');
        $this->addSql('CREATE INDEX IDX_140166BF29C3E95E ON restaurant_join_request (restorer_id)');
        $this->addSql('ALTER TABLE restaurant_join_request ADD CONSTRAINT FK_140166BFB1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE restaurant_join_request ADD CONSTRAINT FK_140166BF29C3E95E FOREIGN KEY (restorer_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE restaurant_join_request_id_seq CASCADE');
        $this->addSql('DROP TABLE restaurant_join_request');
    }
}
