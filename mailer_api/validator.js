const emailRegex = "[a-zA-Z0-9_\\-\\.]+@[a-zA-Z_-]+\\.[a-z]{2,3}";

const recipientsRegex = new RegExp("^( )*"+emailRegex+"(( )*,( )*"+emailRegex+")*( )*$");

const requiredParamByType = {
  confirmAccount: ['firstname','lastname','registerToken','host'],
  accountRestorerValidated: ['firstname','lastname'],
  accountBanned: ['firstname','lastname','reason'],
  accountUnbanned: ['firstname','lastname'],
  restaurantTypeSuggested: ['firstname','lastname','suggestedType'],
  restaurantTypeSuggestionAccepted: ['firstname','lastname','suggestedType'],
  restaurantTypeSuggestionDenied: ['firstname','lastname','suggestedType'],
  restaurantCreated: ['firstname','lastname','name'],
  restaurantValidated: ['firstname','lastname','name'],
  restaurantDenied: ['firstname','lastname','name'],
  restaurantDeletedByAdmin: ['firstname','lastname','name','reason'],
  requestJoinRestaurantSent: ['firstname','lastname','restaurantName'],
  requestJoinRestaurantReceived: ['firstname','lastname','applicantFirstname','applicantLastname','applicantEmail','restaurantName','reason'],
  requestJoinRestaurantAccepted: ['firstname','lastname','restaurantName'],
  requestJoinRestaurantDenied: ['firstname','lastname','restaurantName'],
  bannedFromRestaurant: ['firstname','lastname','restaurantName','reason'],
  reservationDeletedByRestorer: ['firstname', 'lastname', 'reason', 'serviceName', 'restaurantName', 'date', 'nbPersonnes'],
  confirmedReservation: {
    firstname: {required: false},
    lastname: {required: false},
    serviceName: {required: true},
    restaurantName: {required: true},
    date: {required: true},
    nbPersonnes: {required: true}
  },
  leaveRestaurant: ['ownerFirstname', 'ownerLastname', 'restorerLastname', 'restorerFirstname', 'restaurantName', 'reason'],
  forgottenPassword: ['firstname', 'lastname', 'newPassword']
}

const subjectByType = {
  confirmAccount: "BookMyTable - Activez votre compte !",
  accountRestorerValidated: "BookByTable - Compte restaurateur validé !",
  accountBanned: "BookByTable - Vous avez été bannis",
  accountUnbanned: "BookMyTable - Vous avez été débannis !",
  restaurantTypeSuggested: "BookMyTable - Type de restaurant suggéré !",
  restaurantTypeSuggestionAccepted: "BookMyTable - Suggestion de type de restaurant acceptée !",
  restaurantTypeSuggestionDenied: "BookMyTable - Suggestion de type de restaurant refusée",
  restaurantCreated: "BookMyTable - Restaurant créé avec succès !",
  restaurantValidated: "BookMyTable - Restaurant validé !",
  restaurantDenied: "BookMyTable - Restaurant refusé",
  restaurantDeletedByAdmin: "BookMyTable - Restaurant supprimé",
  requestJoinRestaurantSent: "BookMyTable - Demande de candidature envoyée",
  requestJoinRestaurantReceived: "BookMyTable - Nouvelle demande de candidature",
  requestJoinRestaurantAccepted: "BookMyTable - Demande de candidature acceptée",
  requestJoinRestaurantDenied: "BookMyTable - Demande de candidature refusée",
  bannedFromRestaurant: "BookMyTable - Banni du restaurant",
  reservationDeletedByRestorer: "BookMyTable - Réservation supprimée",
  confirmedReservation: "BookMyTable - Confirmation de réservation",
  leaveRestaurant: "BookMyTable - Départ de votre restaurant",
  forgottenPassword: "BookMyTable - Votre nouveau mot de passe"
}

module.exports = {
  validate: function validate(body) {
    return body.recipients !== undefined && recipientsRegex.test(body.recipients) &&
      body.type !== undefined &&
        (
            (
                requiredParamByType[body.type] instanceof Array &&
                !requiredParamByType[body.type].some(requiredParam => body[requiredParam] === undefined)
            ) || (
                typeof(requiredParamByType[body.type]) === "object" &&
                !Object.entries(requiredParamByType[body.type]).some(([param,{required}]) => body[param] === undefined && required)
            )
        )
  },
  subjectByType
}
