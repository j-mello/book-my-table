const Twig = require("twig");

module.exports = function templater(type,params) {
  return new Promise(resolve => {
    Twig.renderFile('./templates/'+type+".twig", params, (err, html) => {
      if (err)
        throw err;
      resolve(html);
    })
  });
}
