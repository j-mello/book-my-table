const validator = require("./validator")
const express = require("express");
const cors = require("cors");
const Mailer = require("./Mailer");
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded());

app.post('/', (req,res) => {
  if (!validator.validate(req.body)) {
    return res.sendStatus(400);
  }
  const mailer = new Mailer();
  mailer.setType(req.body.type);
  mailer.setSubject(validator.subjectByType[req.body.type]);
  for (const recipient of req.body.recipients.split(",")) {
    mailer.addDestinations(recipient.trim());
  }
  mailer.setParams(req.body, true);
  mailer.send().then(success => res.sendStatus(success ? 204 : 500));
});

app.listen(process.env.PORT || 3000);
