const express = require("express");
const cors = require("cors");
const { validate, getContent, getNeededParams } = require("./validator");
const Notification = require("./Models/Notification");
const checkConnectedMiddleWare = require("./Middlewares/checkConnectedMiddleWare");

const app = express();

app.use(cors());
app.use(express.json());


// This route in only accessible from internal APIs
app.post("//", async (req,res) => {
    if (!validate(req.body)) {
        return res.sendStatus(400);
    }
    const neededParams = getNeededParams(req.body);
    const content = getContent(req.body);
    for (const username of req.body.usernames) {
        await Notification.create({
            ...neededParams,
            username,
            content
        });
    }
    res.sendStatus(201);
})


// These routes are accessible from external calls, if user is connected
app.use(checkConnectedMiddleWare);

app.get("//", async (req,res) => {
    res.json(await Notification.find({username: req.user.username}));
});

app.patch("//:notificationId/read", async (req, res) => {
    if (req.params.notificationId.length !== 24)
        return res.sendStatus(400);
    const notification = await Notification.findOne({username: req.user.username, _id: req.params.notificationId});
    if (notification === null) {
        return res.sendStatus(404);
    }
    notification.read = true;
    await notification.save();
    res.sendStatus(200);
});

app.patch("//readAll", async (req, res) => {
    const notifications = await Notification.find({username: req.user.username});
    await notifications.map(async notification => {
        if (!notification.read) {
            notification.read = true
            await notification.save();
        }
    })
    res.sendStatus(200);
})

app.delete("//:notificationId", async (req, res) => {
    if (req.params.notificationId.length !== 24)
        return res.sendStatus(400);
    const notification = await Notification.findOne({username: req.user.username, _id: req.params.notificationId});
    if (notification === null) {
        return res.sendStatus(404);
    }
    await notification.remove();
    res.sendStatus(204);
})



app.listen(process.env.PORT || 3000, () => console.log("Http notification_api server started"));