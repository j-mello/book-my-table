const getJwtPublicKey = require("../libs/getJwtPublicKey");
const jwt = require("jsonwebtoken");

function getBearerToken(req) {
	const authorization = req.headers.authorization;
	const token_matches  = authorization && authorization.match(/(bearer)\s+(\S+)/i);
	return token_matches && token_matches[2];
}

module.exports = function checkConnectedMiddleWare(req,res,next) {
	const token = getBearerToken(req);

	if (!token)
		return res.sendStatus(401)

	getJwtPublicKey().then(async publicKey => {
		const [success,data] = await (() => new Promise(resolve =>
			jwt.verify(token, publicKey, (err, data) => {
				if (err)
					resolve([false,err])
				else
					resolve([true,data]);
			})
		))();
		if (!success) {
			res.status(data.message === "jwt expired" ? 401 : 500).send(data.message)
		} else {
			req.user = data;
			next();
		}
	});
}
