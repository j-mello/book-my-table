const { Schema } = require("mongoose");
const { connect } = require("../Mongo");

const db = connect();

const NotificationSchema = new Schema({
	username: { type: String, required: true },
	restaurantId: { type: Number, required: false }, //If notification is relative to a restaurant,
	reservationId: { type: Number, required: false }, //If notification is relative to a reservation
	type: { type: String, required: true },          // store its id, to allow user to redirect to the restaurant
	content: { type: String, required: true },
	read: { type: Boolean, required: true, default: false },
});

// @ts-ignore
module.exports = db.model('Notification', NotificationSchema);
