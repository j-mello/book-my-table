const requiredParamByType = {
    restaurantTypeSuggested: ['suggestedType'],
    restaurantTypeSuggestionAccepted: ['suggestedType'],
    restaurantTypeSuggestionDenied: ['suggestedType'],
    restaurantCreated: ['name', 'restaurantId'],
    restaurantValidated: ['name', 'restaurantId'],
    restaurantDenied: ['name'],
    restaurantDeletedByAdmin: ['name'],
    requestJoinRestaurantSent: ['restaurantName', 'restaurantId'],
    requestJoinRestaurantReceived: ['applicantFirstname','applicantLastname','applicantEmail','restaurantName', 'restaurantId'],
    requestJoinRestaurantAccepted: ['restaurantName', 'restaurantId'],
    requestJoinRestaurantDenied: ['restaurantName', 'restaurantId'],
    leaveRestaurant: ['lastname', 'firstname', 'restaurantName', 'restaurantId'],
    bannedFromRestaurant: ['restaurantName', 'restaurantId'],
    reservationDeletedByRestorer: ['serviceName', 'restaurantName', 'date', 'nbPersonnes', 'restaurantId'],
    confirmedReservation: ['serviceName', 'restaurantName', 'date', 'nbPersonnes', 'restaurantId', 'reservationId'],
    reservationReceivedWithCustomer: ['lastname', 'firstname', 'restaurantName', 'date', 'serviceName', 'restaurantId', 'reservationId'],
    reservationReceivedWithoutCustomer: ['restaurantName', 'date', 'serviceName', 'restaurantId', 'reservationId'],
    forgottenPassword: []
}

const contentByType = {
    restaurantTypeSuggested: "Vous avez suggéré le type '{{suggestedType}}'",
    restaurantTypeSuggestionAccepted: "Votre suggestion de type '{{suggestedType}}' a été acceptée",
    restaurantTypeSuggestionDenied: "Votre suggestion de type '{{suggestedType}}' a été refusée",
    restaurantCreated: "Votre restaurant '{{name}}' a été créé avec succès!",
    restaurantValidated: "Votre restaurant '{{name}}' a été accepté",
    restaurantDenied: "Votre restaurant '{{name}}' a été refusé",
    restaurantDeletedByAdmin: "Votre restaurant '{{name}}' a été supprimé par un administrateur",
    requestJoinRestaurantSent: "Vous avez demandé à rejoindre le restaurant '{{restaurantName}}'",
    requestJoinRestaurantReceived: "{{applicantFirstname}} {{applicantLastname}} ({{applicantEmail}}) souhaite rejoindre votre restaurant '{{restaurantName}}'",
    requestJoinRestaurantAccepted: "Votre candidature pour le restaurant '{{restaurantName}}' a été acceptée",
    requestJoinRestaurantDenied: "Votre candidature pour le restaurant '{{restaurantName}}' a été refusée",
    leaveRestaurant: "{{firstname}} {{lastname}} a quitté votre restaurant '{{restaurantName}}'",
    bannedFromRestaurant: "Vous avez été banni du restaurant '{{restaurantName}}'",
    reservationDeletedByRestorer: "Votre réservation pour le restaurant '{{restaurantName}}', service '{{serviceName}}' le {{date}} pour {{nbPersonnes}} a été supprimée",
    confirmedReservation: "Votre réservation pour le restaurant '{{restaurantName}}', service '{{serviceName}}' le {{date}} pour {{nbPersonnes}} a été prise",
    reservationReceivedWithCustomer: "{{firstname}} {{lastname}} a réservé au sein du restaurant '{{restaurantName}}', service '{{serviceName}}', le {{date}}",
    reservationReceivedWithoutCustomer: "Une réservation a été prise au sein du restaurant '{{restaurantName}}', service '{{serviceName}}', le {{date}}",
    forgottenPassword: "Votre mot de passe a été réinitialisé"
}

module.exports = {
    validate: (body) =>
        body.usernames instanceof Array &&
        body.type !== undefined && requiredParamByType[body.type] !== undefined &&
        !requiredParamByType[body.type].some(param => body[param] === undefined),
    getContent: (body) => {
        let content = contentByType[body.type];
        while (content !== (content = content.replace(/{{( )*[a-zA-Z0-1]+( )*}}/i, (match) =>
            body[match.substring(2, match.length - 2).trim()] ?? match
        ))) {}
        return content
    },
    getNeededParams: (body) =>
        Object.entries(body).reduce((acc,[key,value]) => ({
            ...acc,
            ...((key === "type" || requiredParamByType[body.type].includes(key)) ? {[key]: value} : {})
        }), {})
}