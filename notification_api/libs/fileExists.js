const {promises: fs, constants: {R_OK}} = require("fs");

module.exports = async function fileExists(path) {
	try {
		await fs.access(path, R_OK);
		return true;
	} catch (e) {
		return false;
	}
}
