import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    meta: {
      title: 'Book My Table Welcome Page',
    },
    component: () => import('../views/Home.vue')
  },
  {
    path: '/login',
    name: 'Login',
    meta: {
      title: 'Book My Table Login Page',
    },
    component: () => import('../views/Login.vue')
  },
  {
    path: '/forgotten_password',
    name: "ForgottenPassword",
    meta: {
      title: 'Book My Table Recovery Page',
    },
    component: () => import('../views/ForgottenPassword')
  },
  {
    path: '/register',
    name: 'Register',
    meta: {
      title: 'Book My Table Refister Page',
    },
    component: () => import('../views/Register.vue')
  },
  {
    path: '/registered',
    name: 'Registered',
    component: () => import("../views/Registered")
  },
  {
    path: '/validate/:token',
    name: 'Validate',
    component: () => import("../views/Validate")
  },
  {
    path: '/myaccount',
    name: 'Account',
    meta: {
      title: 'Book My Table Account Page',
    },
    component: () => import("../views/Account")
  },
  {
    path: '/reservations',
    name: 'Reservations',
    meta: {
      title: 'Book My Table Reservation Page',
    },
    component: () => import("../views/Reservations/Reservations")
  },
  {
    path: '/restaurants',
    name: 'Restaurants',
    meta: {
      title: 'Book My Table Restaurant Page',
    },
    component: () => import("../views/Restaurants/Restaurants")
  },
  {
    path: '/usermanagement',
    name: 'UserManagement',
    meta: {
      title: 'Book My Table Management Page',
    },
    component: () => import("../views/Admin/Users/UserManagement")
  },
  {
    path: '/suggestions',
    name: 'Suggestions',
    meta: {
      title: 'Book My Table Suggestion Page',
    },
    component: () => import("../views/Admin/SuggestionsTypeRestaurant/Suggestions")
  },
  {
    path: '*',
    name: 'Home When Page Not Found',
    meta: {
      title: 'Book My Table Welcome Page',
    },
    component: () => import('../views/Home.vue')
  },
]

const router = new VueRouter({
  routes
})

export default router
