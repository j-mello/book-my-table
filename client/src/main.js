import Vue from 'vue'
import App from './App.vue'
import router from './router'
import '@/assets/css/tailwind.css'
import {library} from '@fortawesome/fontawesome-svg-core'
import {
    faLock,
    faCheck,
    faTimes,
    faEye,
    faDoorClosed,
    faTrash,
    faChevronLeft,
    faTimesCircle,
    faAngleRight,
    faAngleDown,
    faCrown,
    faEdit,
    faArrowUp,
    faArrowDown,
    faBell,
    faExternalLinkAlt,
    faStar,
    faDoorOpen
} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import Vuemik from "@/components/Form/Vuemik";
import Field from "@/components/Form/Field";
import Modal from "@/components/Modal";
import RecursiveShowMenus from "@/views/Menus/RecursiveShowMenus";
import Card from "@/components/Card";
import Button from "@/components/Form/Button";

library.add(
    faLock,
    faCheck,
    faTimes,
    faEye,
    faDoorClosed,
    faTrash,
    faChevronLeft,
    faTimesCircle,
    faAngleRight,
    faAngleDown,
    faCrown,
    faEdit,
    faArrowUp,
    faArrowDown,
    faBell,
    faExternalLinkAlt,
    faStar,
    faDoorOpen
);

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component("Vuemik", Vuemik);
Vue.component("Field", Field);
Vue.component("Modal", Modal);
Vue.component("RecursiveShowMenus", RecursiveShowMenus);
Vue.component("Card", Card);
Vue.component("Button", Button);

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);
    const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.title);

    if(nearestWithTitle) {
        document.title = nearestWithTitle.meta.title;
    } else if(previousNearestWithMeta) {
        document.title = previousNearestWithMeta.meta.title;
    }
    next()
})

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')
