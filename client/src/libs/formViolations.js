export function getViolations(violationsList, violationState) {
    clearViolations(violationState);
    for (const {propertyPath,message} of violationsList) {
        if (violationState[propertyPath] === undefined)
            continue;
        violationState[propertyPath].push(message)
    }
}

export function clearViolations(violationState) {
    for (let key in violationState) {
        violationState[key] = [];
    }
}