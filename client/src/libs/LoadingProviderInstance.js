let _loadingProvider = null;

export function setLoadingProvider(loadingProvider) {
    _loadingProvider = loadingProvider;
}

export function getLoadingProvider() {
    return _loadingProvider;
}