export const formatFormTimeToISO = timeStr => {
    const h = parseInt(timeStr.split(":")[0]);
    const m = parseInt(timeStr.split(":")[1]);
    const date = new Date(0);
    date.setHours(h);
    date.setMinutes(m);
    return date.toISOString();
}

const addMissingZeros = (num, n = 2) => {
    num = num.toString();
    while (num.length < n) {
        num = '0'+num;
    }
    return num;
}

export const formatISODateFrenchFormat = (dateStr) => {
    const date = new Date(dateStr);
    return addMissingZeros(date.getDate())+"/"+addMissingZeros(date.getMonth()+1)+"/"+date.getFullYear();
}
export const formatISODateEnglishFormat = (dateStr) => {
    const date = new Date(dateStr);
    return +date.getFullYear()+"-"+addMissingZeros(date.getMonth()+1)+"-"+addMissingZeros(date.getDate());
}

export const formatISOTimeForForm = (dateStr) =>  {
    const date = new Date(dateStr);
    return addMissingZeros(date.getHours())+":"+addMissingZeros(date.getMinutes());
};

export const formatISOTimeForList = (dateStr) => {
    const date = new Date(dateStr);
    return addMissingZeros(date.getHours()) + "h" + addMissingZeros(date.getMinutes());
};