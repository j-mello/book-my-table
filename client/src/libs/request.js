import {getLoadingProvider} from "@/libs/LoadingProviderInstance";

export default function request(url,params = {}, showLoading = false) {
    params.headers = {
        Accept: "application/json",
        "Content-Type": "application/ld+json",
        ...(params.headers||{}),
    }
    const session = JSON.parse(localStorage.getItem("session"));
    if (session !== null) {
        const token = session.token;
        params.headers.Authorization = 'Bearer ' + token;
    }
    url = url.replace(/{( )*[a-zA-Z0-1]+( )*}/i, function (match) {
        return session ? session[match.replace("{","").replace("}","")]??match : match
    });
    if (showLoading) getLoadingProvider().showLoading();
    return fetch(url,params)
        .then(async res => {
            if (showLoading) getLoadingProvider().hideLoading();
            if (res.status === 401 && url.split("/")[3] !== "login")  {
                localStorage.removeItem("session");
                location.href = "/";
            } else {
                return res;
            }
        });
}