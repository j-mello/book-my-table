import {clearViolations, getViolations} from "@/libs/formViolations";

export const reactFromAPIDelete = (res,violations,createAlert,successMessage = null, timeout = 5000) =>
    reactFromAPI(res,violations,createAlert,successMessage, 204, timeout)

export const reactFromAPIPost = (res,violations,createAlert,successMessage = null, timeout = 5000) =>
    reactFromAPI(res,violations,createAlert,successMessage, 201, timeout)

export const reactFromAPIPut = (res,violations,createAlert,successMessage = null, timeout = 5000) =>
    reactFromAPI(res,violations,createAlert,successMessage, 200, timeout)

const reactFromAPI = async (res,violations,createAlert,successMessage,successCode, timeout) => {
    let json;
    switch (res.status) {
        case successCode:
            clearViolations(violations);
            if (successMessage !== null) {
                createAlert({
                    color: "green",
                    text: successMessage,
                    icon: "check"
                }, timeout);
            }
            return true;
        case 422:
            json = await res.json();
            getViolations(json.violations, violations);
            return false;
        default:
            try {
                json = await res.json();
            } catch (e) {
                json = {}
            }
            createAlert({
                color: "red",
                text: json.detail??"Une erreur est survenue",
                icon: "times"
            });
            return false;
    }
}