const generateFilterQuery =
        filters => Object.entries(filters).reduce((acc,[name,value]) =>
            acc+((value !== "" && value !== null) ? (acc !== '' ? '&' : '')+name+"="+value : "")
        , "")

export default generateFilterQuery;