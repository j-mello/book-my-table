import request from "@/libs/request";
import {url} from "@/libs/global";

export const getNotifications = () =>
    request(url+"/notification")

export const deleteNotification = (id) =>
    request(url+"/notification/"+id, {
        method: "DELETE"
    })

export const readAllNotification = () =>
    request(url+"/notification/readAll", {
        method: "PATCH"
    })
