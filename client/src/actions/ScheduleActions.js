import request from "@/libs/request";
import {formatFormTimeToISO} from "@/libs/timeFormatter";
import {url} from "@/libs/global";

export const getSchedules = restaurantId =>
    request(url+"/restaurants/"+restaurantId+"/schedules")

export const createSchedule = (restaurantId,values) =>
    request(url+"/schedules", {
        method: "POST",
        body: JSON.stringify({
            ...values,
            start: formatFormTimeToISO(values.start),
            ending: formatFormTimeToISO(values.ending),
            nbPlace: parseInt(values.nbPlace),
            restaurantId
        })
    }, true)

export const putSchedule = (scheduleId,values) =>
    request(url+"/schedules/"+scheduleId, {
        method: "PUT",
        body: JSON.stringify({
            ...values,
            start: formatFormTimeToISO(values.start),
            ending: formatFormTimeToISO(values.ending),
            nbPlace: parseInt(values.nbPlace)
        })
    }, true)

export const deleteSchedule = (scheduleId) =>
    request(url+"/schedules/"+scheduleId, {
        method: "DELETE"
    }, true)
