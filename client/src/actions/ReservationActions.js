import request from "@/libs/request";
import {url} from "@/libs/global";

export const getCustomerReservations = () =>
    request(url+"/users/{id}/reservations")

export const getRestaurantReservations = restaurantId =>
    request(url+"/restaurants/"+restaurantId+"/reservations")

export const getScheduleReservations = scheduleId =>
    request(url+"/schedules/"+scheduleId+"/reservations")

export const deleteReservation = (reservationId, reason = null) =>
    request(url+"/reservations/"+reservationId, {
        method: "DELETE",
        ...(reason != null ? {body: JSON.stringify({reason})} : {})
    }, true)

export const bookTable = (date, nbpersonnes, specifiedCustomerID, scheduleID, comment, emailToConfirm) =>
    request(url+"/reservations", {
        method: "POST",
        body: JSON.stringify({
            date, 
            nbpersonnes: parseInt(nbpersonnes), 
            specifiedCustomerID, 
            scheduleID, 
            comment,
            emailToConfirm
        })
    }, true)

export const editReservation = (id, date, nbpersonnes, scheduleID, comment) =>
    request(url+"/reservations/"+id, {
        method: "PUT",
        body: JSON.stringify({
            date,
            nbpersonnes: parseInt(nbpersonnes),
            scheduleID: parseInt(scheduleID),
            comment
        })
    }, true)
