import request from "@/libs/request";
import {url} from "@/libs/global";

export const getRestaurantMenus = restaurantId =>
    request(url+"/menu/restaurants/"+restaurantId)

export const postSubMenu = (restaurantId,menuId,values) =>
    request(url+"/menu/menus/"+restaurantId+"/"+menuId, {
        method: "POST",
        headers: {
          "Content-Type": 'application/json'
        },
        body: JSON.stringify(values)
    }, true)

export const postMenu = (restaurantId,values) =>
    request(url+"/menu/menus/"+restaurantId, {
        method: "POST",
        headers: {
            "Content-Type": 'application/json'
        },
        body: JSON.stringify(values)
    }, true)

export const deleteMenu = (restaurantId,menuId) =>
    request(url+"/menu/menus/"+restaurantId+"/"+menuId, {
        method: "DELETE"
    }, true)

export const putMenu = (restaurantId,menuId,values) =>
    request(url+"/menu/menus/"+restaurantId+"/"+menuId, {
        method: "PUT",
        headers: {
            "Content-Type": 'application/json'
        },
        body: JSON.stringify(values)
    }, true)

export const upMenu = (restaurantId,menuId) =>
    request(url+"/menu/menus/"+restaurantId+"/"+menuId+"/up", {
        method: "PUT"
    }, true);

export const downMenu = (restaurantId,menuId) =>
    request(url+"/menu/menus/"+restaurantId+"/"+menuId+"/down", {
        method: "PUT"
    }, true);

export const postElement = (restaurantId,menuId,values) =>
    request(url+"/menu/menus/"+restaurantId+"/"+menuId+"/elements", {
        method: "POST",
        headers: {
            "Content-Type": 'application/json'
        },
        body: JSON.stringify({
            ...values,
            price: parseFloat(values.price)
        })
    }, true)

export const deleteElement = (restaurantId,menuId,elementId) =>
    request(url+"/menu/menus/"+restaurantId+"/"+menuId+"/elements/"+elementId, {
        method: "DELETE"
    }, true)

export const putElement = (restaurantId,menuId,elementId,values) =>
    request(url+"/menu/menus/"+restaurantId+"/"+menuId+"/elements/"+elementId, {
        method: "PUT",
        headers: {
            "Content-Type": 'application/json'
        },
        body: JSON.stringify({
            ...values,
            price: parseFloat(values.price)
        })
    }, true)

export const upElement = (restaurantId,menuId,elementId) =>
    request(url+"/menu/menus/"+restaurantId+"/"+menuId+"/elements/"+elementId+"/up",{
        method: "PUT"
    }, true)

export const downElement = (restaurantId,menuId,elementId) =>
    request(url+"/menu/menus/"+restaurantId+"/"+menuId+"/elements/"+elementId+"/down",{
        method: "PUT"
    }, true)
