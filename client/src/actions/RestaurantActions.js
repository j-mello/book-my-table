import request from "@/libs/request";
import {url} from "@/libs/global";
import generateFilterQuery from "@/libs/generateFilterQuery";

export const getRestaurants = (filters = {}, page = 1, paginate = true) =>
    request(url+"/restaurants?"+generateFilterQuery(filters)+(Object.keys(filters).length > 0 ? '&' : '')+'page='+page+'&paginate='+(paginate ? 'true' : 'false'))

export const getOneRestaurant = id =>
    request(url+"/restaurants/"+id, {}, true)

export const getMemberedRestaurants = (filters = {}, page = 1, paginate = true) =>
    request(url+"/users/{id}/restaurants?"+generateFilterQuery(filters)+(Object.keys(filters).length > 0 ? '&' : '')+'page='+page+'&paginate='+(paginate ? 'true' : 'false'))

export const getOwnedRestaurants = (filters = {}, page = 1, paginate = true) =>
    request(url+"/users/{id}/owned_restaurants?"+generateFilterQuery(filters)+(Object.keys(filters).length > 0 ? '&' : '')+'page='+page+'&paginate='+(paginate ? 'true' : 'false'))

export const createRestaurant = (values) =>
    request(url+"/restaurants", {
        method: "POST",
        body: JSON.stringify({...values, nbPlace: parseInt(values.nbPlace)})
    }, true)

export const putRestaurant = (id,values) =>
    request(url+"/restaurants/"+id, {
        method: "PUT",
        body: JSON.stringify({...values, nbPlace: parseInt(values.nbPlace)})
    }, true)

export const putRestaurantTypes = (id,types) =>
    request(url+"/restaurants/"+id+"/types", {
        method: "PUT",
        body: JSON.stringify({types: types.join(',')})
    }, true)

export const deleteRestaurant = (id, reason = null) =>
    request(url+"/restaurants/"+id, {
        method: "DELETE",
        ...(reason !== null ? {body: JSON.stringify({reason})} : {})
    }, true)

export const validateRestaurant = id =>
    request(url+"/restaurants/"+id+"/validate", {
        method: "POST",
        body: "{}"
    }, true)

export const denyRestaurant = id =>
    request(url+"/restaurants/"+id+"/deny", {
        method: "POST",
        body: "{}"
    }, true)

export const checkIfIsMember = id =>
    request(url+"/restaurants/"+id+"/is_member")

export const getMembers = id =>
    request(url+"/restaurants/"+id+"/users")

export const banMember = (restaurantId,restorerId,reason) =>
    request(url + "/restaurants/" + restaurantId + "/member", {
        method: "DELETE",
        body: JSON.stringify({
            restorerId,
            reason
        })
    }, true)

export const askToJoin = (restaurantId,reason) =>
    request(url+"/restaurants/"+restaurantId+"/join", {
        method: "POST",
        body: JSON.stringify({reason})
    }, true)

export const leave = (restaurantId,reason) =>
    request(url+"/restaurants/"+restaurantId+"/leave", {
        method: "DELETE",
        body: JSON.stringify({reason})
    }, true)
