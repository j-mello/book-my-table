import request from "@/libs/request";
import {url} from "@/libs/global";

export const getJoinRequests = restaurantId =>
    request(url+"/restaurants/"+restaurantId+"/restaurant_join_requests")

export const acceptJoinRequest = joinRequestId =>
    request(url+"/restaurant_join_requests/"+joinRequestId+"/accept", {
        method: "POST",
        body: "{}"
    }, true)

export const denyJoinRequest = joinRequestId =>
    request(url+"/restaurant_join_requests/"+joinRequestId+"/deny", {
        method: "POST",
        body: "{}"
    }, true)
