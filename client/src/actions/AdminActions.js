import request from "../libs/request";
import {url} from "@/libs/global";

import generateFilterQuery from "@/libs/generateFilterQuery";

export const getUsers = (filters) =>
    request(url+'/users?'+generateFilterQuery(filters))

export const banUnbanUser = (id, banned, reason = null) =>
    request(url+'/users/'+id, {
        method: 'PATCH',
        body: JSON.stringify( {
            banned,
            ...(reason !== null ? {reason} : {})
        }),
        headers: {
            'Content-Type': 'application/merge-patch+json'
        }
    }, true)

export const validateRestorer = id =>
    request(url+"/users/"+id, {
        method: "PATCH",
        body: JSON.stringify({
            restorerValidated: true
        }),
        headers: {
            'Content-Type': 'application/merge-patch+json'
        }
    }, true)

export const getSuggestions = () => request(url+'/restaurant_type_suggestions')

export const getUserById = (id) => request(url+'/users/'+id, {}, true)

export const validSuggestionsTypeById = (id) => request(url+'/restaurant_type_suggestions/'+id+'/accept', {
    method: 'POST',
    body: JSON.stringify( {})
}, true)

export const denySuggestionsTypeById = (id) => request(url+'/restaurant_type_suggestions/'+id+'/deny', {
    method: 'POST',
    body: JSON.stringify( {})
}, true)
