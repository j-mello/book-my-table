import request from "@/libs/request";
import {url} from "@/libs/global";

export const getRestaurantsTypes = () =>
    request(url+"/restaurant_types")

export const proposeNewType = (name) =>
	request(url+"/restaurant_type_suggestions/propose_new", {
		method: "POST",
		body: JSON.stringify({name})
	}, true)
