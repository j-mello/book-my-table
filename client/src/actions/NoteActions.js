import request from "@/libs/request";
import {url} from "@/libs/global";

export const getAverageNote = restaurantId =>
	request(url+"/note/"+restaurantId+"/average")

export const getNote = restaurantId =>
	request(url+"/note/"+restaurantId+"/note")

export const setNote = (restaurantId,note) =>
	request(url+"/note/"+restaurantId+"/note", {
		method: "PUT",
		headers: {
			"Content-Type": 'application/json'
		},
		body: JSON.stringify({note})
	})

export const deleteNote = restaurantId =>
	request(url+"/note/"+restaurantId+"/note", {
		method: 'DELETE'
	})
