import request from "@/libs/request";
import {url} from "@/libs/global";

export const register = (values) =>
    request(url+"/users", {
        method: "POST",
        body: JSON.stringify({
          ...values,
          roles: values.isRestorer ? ['ROLE_RESTORER'] : ['ROLE_USER']
        })
      }, true)

export const login = (values) =>
    request(url+"/login", {
        method: "POST",
        body: JSON.stringify(values)
    }, true)

export const validate = (registerToken) =>
    request(url+"/users/confirm_account", {
        method: "PATCH",
        headers:
        {
            "Content-Type": "application/merge-patch+json"
        },
        body: JSON.stringify(
            [
                {
                    registerToken: registerToken
                }
            ]
        )
    })

export const me = (token) =>
    fetch(url+"/me",{
        headers: {
            Authorization: 'Bearer ' + token
        }
    }, true)

export const putNames = values =>
    request(url+"/users/{id}", {
        method: "PUT",
        body: JSON.stringify(values)
    }, true)

export const patchPassword = plainPassword =>
    request(url+"/users/{id}/change_password", {
        method: "PATCH",
        body: JSON.stringify({plainPassword}),
        headers: {
            "Content-Type": "application/merge-patch+json"
        }
    }, true)

export const findByEmail = email =>
    request(url+'/users/find_by_email?email='+email)

export const resetPassword = email =>
    request(url+"/users/forgotten_password", {
        method: "POST",
        body: JSON.stringify({email})
    })
