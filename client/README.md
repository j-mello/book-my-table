# client

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### .env
copy HOST variable located into .env-example file and paste it into .env file 

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
